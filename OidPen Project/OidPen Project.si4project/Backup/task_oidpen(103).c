#include "file_operate.h"
#include "sdk_cfg.h"
#include "task_manager.h"
#include "task_common.h"
#include "task_oidpen_key.h"
#include "two_wire.h"
#include "msg.h"
#include "warning_tone.h"
#include "music_player.h"
#include "fat_io.h"
#include "file_io.h"

#include "dev_manage.h"
#include "fs_io.h"

#include "OidIndexSoundFunctionLib.h"
#include "SystemSoundPlay.h"
#include "ParsingIndexTypeLib.h"
#include "GameVariableDef.h"
#include "GameManyGroupSound.h"


typedef struct __FILE_OPERATE FILE_OPERATE;
extern tbool task_music_skip_check(void **priv);


typedef struct CardBookRangetag{
    u32 RangeStart;            		//range start
	u32 RangeEnd;					//range end

} CARDBOOKRANGE;

//记录书本文件进入的信息，以便要切换书本时查找书本对应的是那个文件
typedef struct bookEntrytag{
    u32 OID3BookIndex;            	//OID3 BOOK ENTYR CODE
	u32 OID2BookIndex;				//OID2 BOOK ENTYR CODE
	u32 CardBookRangeCnt;			//CARD BOOK RANGE ENTYR RANGE COUNT
	CARDBOOKRANGE CardBookRange[10];//CARD BOOK RANGE
	char pcFileName[32];			//file name 128

} BOOKENTRYINFO;



WORD g_wActiveMode;
FS_HANDLE pOidPlayFile;
FS_HANDLE pSystemFile;
u32 m_dwOIDPhyIndex;
WORD m_wIndexType;
u32 m_dwIndexLogic;
DWORD g_dwPreElementPhyIndex;
BYTE m_isPlayAllOpen;

//WORD m_wGameType;
//u32 m_dwStartAddr;
//u32 m_dwEndAddr;
//u32 m_xTime;
//BYTE m_pBuf[512];

BOOKENTRYINFO m_BookFileList[10];	//自行按平台资源设置，文件管理

//idle要自设添加，不在ilde中计时										<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
WORD m_wIdleTimeOutSet;
WORD m_wIdleTimeExitGameSet;
WORD m_wIdleTimeX;
WORD m_wIdleTimeCnt;







/* 扫描文件夹下所有bnf文件，并将文件的信息读出建立书本列表，m_BookFileList[]
以便在文件个数较多时，加快选书时间 */
void ScanDirAndBuildBookFileList(MUSIC_PLAYER* obj)
{
    log_printf("ScanDirAndBuildBookFileList in ....\n");

	u32 dwOID3BookCode = 0;
	u32 dwOID2BookCode=0;
	WORD wRangeCnt = 0;
	WORD wPos;
	WORD wRet;
	u32 dwRangeStart,dwRangeEnd;
	FILE *pFile;

	char pcFileName[32];

	WORD wFileCnt;
	WORD i = 0;
	WORD wVer =0;
	WORD r;

    _FIL_HDL *f_p = file_operate_get_file_hdl(obj->fop);
    _FS_HDL  *fs_p = file_operate_get_fs_hdl(obj->fop);

    pFile = (FILE*)&(obj->fop->fop_file);

	//打开文件夹，请自行在自己的平台上打开文件所在的文件夹
	//取得文件总数，请自行在自己的平台上取得文件夹中XXX文件数量
	wFileCnt = file_operate_get_file_total(obj->fop);
    log_printf("wFileCnt:%d \n",  wFileCnt);

#if 0
    for(i=1;i<(wFileCnt+1);i++)///文件从序号1开始
    {

		file_operate_set_file_number(obj->fop,i);
		file_operate_op(obj->fop, FOP_OPEN_FILE_BYNUM, NULL, NULL);

		void * f_path=NULL;
		file_operate_get_file_name(obj->fop,&f_path);
		printf("%d file_name :%s \n ",i, f_path);

//		char pcFileName[32];
//		file_operate_get_file_name(obj->fop,pcFileName);
//		printf("%d file_name :%s \n ",i, pcFileName);

//		fs_close(&(obj->fop->fop_file->file_hdl));

    }
#endif

#if 1


		//逐个扫描所有文件，建立列表
	for(i = 1;i < (wFileCnt+1);i++)
	{
		//取得第i个文件的名字，请自行在自己的平台上取得文件名
//		memset(pcFileName, 0, sizeof(pcFileName));

		file_operate_set_file_number(obj->fop, i);
		file_operate_op(obj->fop, FOP_OPEN_FILE_BYNUM, NULL, NULL);

		void* f_path=NULL;
		file_operate_get_file_name(obj->fop,&f_path);
		printf("%d file_name :%s \n ",i, f_path);


        //打开第i个书本文件
		//pFile = fopen(pcFileName,"r");//打开书本，，请自行在自己的平台上打开文件函数打开文件
		if(pFile == 0)
		{
		    log_printf("fs_open err!\n");
			continue;
		}


		//初始range总数
		wRangeCnt = 0;
		m_BookFileList[i].CardBookRangeCnt = wRangeCnt;

		//保存对应的文件名
		//memcpy(m_BookFileList[i].pcFileName,pcFileName,sizeof(pcFileName));

		//读版本号
		FS_Seek((DWORD)&pFile, 0, FS_FILE_BEGIN);
		FS_Read((DWORD )&pFile, (u8*)&wVer, 2, &r);

        //fs_seek(pFile->fs_hdl, 0, FS_FILE_BEGIN);
		//fs_read(pFile->fs_hdl, (u8*)&wVer, 2);
        log_printf("wVer:0x%04x \n", wVer);

		if(wVer >= 8)
		{
			//
			WORD pCardBookCntBuf[1];
			CARDBOOKRANGE pCardBookBuf[10];
			//读书码
			fs_seek(f_p ,8,0);
			fs_read(f_p, (u8*)dwOID2BookCode,4);
            log_printf("dwOID2BookCode:%x \n", dwOID2BookCode);

			fs_seek(f_p ,12,0);
			fs_read(f_p, (u8*)dwOID3BookCode, 4);
            log_printf("dwOID2BookCode:%x \n", dwOID3BookCode);

			//读范围
			fs_seek(f_p ,212,0);
			fs_read(f_p,(u8*)pCardBookCntBuf,2);
            log_printf("dwOID2BookCode:%x \n", dwOID3BookCode);

			fs_seek(f_p ,212+2,0);
			fs_read(f_p, (u8*)pCardBookBuf,pCardBookCntBuf[0]*4);


			//保存到列表
			m_BookFileList[i].OID3BookIndex = dwOID3BookCode;
			m_BookFileList[i].OID2BookIndex = dwOID2BookCode;
			wRangeCnt = pCardBookCntBuf[0];
			memcpy(m_BookFileList[i].CardBookRange,pCardBookBuf,wRangeCnt*4);

			//保存range总数
			m_BookFileList[i].CardBookRangeCnt = wRangeCnt;


		}
		else
		{
/*
            //;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
			//取得文件中记录的书码，分OID2,OID3两种
			pFile = NULL;//obj->fop->fop_file;

			//GetBookPhyIndex(pFile, &dwOID3BookCode, &dwOID2BookCode);
			//如类型为Idx_BOOK，对应的是OID3
			//Idx_BOOKOID2对应OID2
			//即如果类型为Idx_BOOKOID2，则要拿收到的码跟dwOID2BookCode比对，以确定那那一个文件

			//保存到列表
			m_BookFileList[i].OID3BookIndex = dwOID3BookCode;
			m_BookFileList[i].OID2BookIndex = dwOID2BookCode;
			//;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
			//取得文件中直接进入书本文件的range
			//检查文件合法性，并初始化
			//检查书本
			wRet = CheckBookFile(pFile);
			if(wRet == 0)
			{
				//检查通过，初始化
				wRet = InitBookFile(pFile);
				if(wRet == 1)
				{
					//初始化成功

				}
				else
				{
					continue;
				}

			}
			else
			{
				continue;
			}

			//取得书本的范围，此范围是在类型是Idx_CARDBOOK时，于此确定码值在那个文件中的范围中
			//以确定打开那个文件
			wPos = GetCardBookFirstIndexRange(pFile,&dwRangeStart,&dwRangeEnd);
			while(wPos)
			{
				//保存一组范围
				//dwRangeStart,pdwRangeEnd
				wRangeCnt++;////????
				m_BookFileList[i].CardBookRange[wRangeCnt].RangeStart = dwRangeStart;
				m_BookFileList[i].CardBookRange[wRangeCnt].RangeEnd = dwRangeEnd;
				wRangeCnt++;

				//取得下一组
				wPos =  GetCardBookNextIndexRange(pFile,wPos,&dwRangeStart, &dwRangeEnd);

			}
			//保存range总数
			m_BookFileList[i].CardBookRangeCnt = wRangeCnt;
*/
		}
		//下一个文件
		fs_close(f_p);
	}
//
#endif
}



/* 返回是列表内的第几本书，找不到返回0xffff */
WORD FindFileByBookFileList(WORD wIndexType,u32 dwOidPhyIndex)
{

	WORD i = 0;

	switch(m_wIndexType)
	{
		//按类型查找
		case Idx_BOOK :
		{
			for(i = 0;i < 320;i++)
			{
				//看是码值是否相等
				if(dwOidPhyIndex == m_BookFileList[i].OID3BookIndex)
				{
					return i;
				}
			}
			break;
		}
		case Idx_BOOKOID2 :
		{
			for(i = 0;i < 320;i++)
			{
				//看是码值是否相等
				if(dwOidPhyIndex == m_BookFileList[i].OID2BookIndex)
				{
					return i;
				}
			}
			break;
		}
		case Idx_CARDBOOK :
		{
			for(i = 0;i < 320;i++)
			{
				WORD j = 0;
				for(j = 0; j < m_BookFileList[i].CardBookRangeCnt;j++)
				{
					//看是否在范围内
					if(dwOidPhyIndex >= m_BookFileList[i].CardBookRange[j].RangeStart
						&& (dwOidPhyIndex <= m_BookFileList[i].CardBookRange[j].RangeEnd)
						)
					{
						return i;
					}
				}
			}
			break;
		}
	}

	return 0xffff;
}


void parse_receive_data(u8 *data)
{
    if(data)
    {
        m_dwOIDPhyIndex = ((u32)data[4]<<24)|((u32)data[5]<<16)|((u32)data[6]<<8)|(u32)data[7];
        //log_printf("m_dwOIDPhyIndex:0x%08x\n", m_dwOIDPhyIndex);
    }
}


static void *task_oidpen_init(void *priv)
{
    log_printf(".........%s in ........\n\n\n",__func__);
#if 0
    if (priv  == (void *)sd0) {
        puts("[PRIV SD0]\n");
    } else if (priv == (void *)sd1) {
        puts("[PRIV SD1]\n");
    } else if (priv == (void *)usb) {
        puts("[PRIV USB]\n");
    } else {
        puts("[PRIV CACHE]\n");
    }
#endif

    MUSIC_PLAYER *obj = NULL;

    fat_init();
	fat_mem_init(NULL, 0);

    obj = bnf_player_creat();


  //  tone_play(TONE_RADIO_MODE, 0);
    return obj;
}


static void task_oidpen_exit(void **priv)
{
    log_printf("%s out.\n",__func__);
    music_player_destroy((MUSIC_PLAYER **)priv);
    task_clear_all_message();
    fat_del();
}

void task_oidpen_deal(void *hdl)
{
    log_printf("%s in ........\n\n\n",__func__);

    u8 data[]={0};
	WORD wRet;
    int msg = NO_MSG;
    int msg_error = MSG_NO_ERROR;

	g_wActiveMode = OID_PLAY_MODE;//点读;

	pOidPlayFile = 0;
	pSystemFile = 0;//打开系统文件
	m_dwOIDPhyIndex = 0xffffffff;

	//建立书本列表
	//>>要根据实际情况选择建立 的机时，如文件较多，这个时间可能会比较久
	MUSIC_PLAYER *obj = (MUSIC_PLAYER *)hdl;

	ScanDirAndBuildBookFileList(obj);

    while(TRUE)
    {

        msg_error = task_get_msg(0, 1, &msg);

        if (task_common_msg_deal(hdl, msg) == false) {
            log_printf("msg exit, return ...............\n");
            break;
        }

        if(master_readfrom_slave(data))
        {
            parse_receive_data(data);
           // GetIndexType(pOidPlayFile, pSystemFile, m_dwOIDPhyIndex,&m_wIndexType,&m_dwIndexLogic);
            log_printf("m_wIndexType:0x%04x\n", m_wIndexType);
        }

        switch(m_wIndexType)
        {
            case Idx_BOOK    :
            case Idx_BOOKOID2:
            case Idx_CARDBOOK:
                break;
            {



                break;
            }

            case Idx_GAME:
            {


                break;
            }







            default:
                break;
        }


        switch(g_wActiveMode)
        {
            case OID_PLAY_MODE:
            {
                break;
            }
            #if 0
            case OID_READAFTER_MODE:
            {

                break;
            }

            case GAME_PLAY_MODE:
            {

                break;
            }

            case MP3_PLAY_MODE:
            {
            }
            #endif
            default:
                break;
        }

        #if 0
		//mp3 播放buffer填充过程
		if(m_dwStartAddr+m_xTime*512 < m_dwEndAddr)
		{//看是否播放到结束位置了

			if(播放平台Mp3Buffer是空)
			{
				//读一笔数据,每次读512B,因解密一次只能是512B
				fseek(pOidPlayFile,m_dwStartAddr+m_xTime*512,0);//到mp3的开始位置，准备读数据
				fread(m_pBuf,1,512,pOidPlayFile);
				//解密
				AfterMP3FillBuffer((WORD*)m_pBuf,m_dwStartAddr+m_xTime*512);

				//解密后数据仍在传入的bufffer中，直接传给播放的buffer

				m_xTime++;		//每读一次要加1
			}
		}
        #endif

    }//end while(TRUE)



}



const TASK_APP task_oidpen_info = {
    .skip_check = task_music_skip_check,
    .init 		= task_oidpen_init,
    .exit 		= task_oidpen_exit,
    .task 		= task_oidpen_deal,
    .key 		= &task_oidpen_key,
};

