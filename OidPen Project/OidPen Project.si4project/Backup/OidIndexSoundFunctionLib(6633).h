
#ifndef __OID_LIB__
#define __OID_LIB__

//#include "fs_gprot.h"
#include <stdio.h>
//#include "ff.h"
//#include "fs/fatfs/ff.h"

//#include "RWFifo.h"
//#include "esp_log.h"
#include "RxIndex.h"
#include "ErrCodeDef.h"
#include "CodeRangeDef.h"
//#include "EnableFunctionDef.h"
//#include "EnableFunctionDef-XueXian.h"
#include "EnableFunctionDef-ABCM.h"
#include "MaxBufDef.h"
#include "DebugDef.h"
#include "IndexFunctionCodePhyIndex.h"

//#define WORD u32

//typedef unsigned short    	BOOL;          	// Unsigned 16 bit quantity
typedef unsigned short    	u16;          	// Unsigned 16 bit quantity
//typedef unsigned long    	u32;          	// Unsigned 16 bit quantity
typedef unsigned short    	WORD;          	// Unsigned 16 bit quantity
//typedef u32    	u32;          				// Unsigned 16 bit quantity
typedef unsigned char   	BYTE;          	// Unsigned 8 bit quantity
typedef unsigned long		DWORD;

//Oid data type
typedef enum OidDataTypeTag
{
	DATATYPE_OIDCMD,			//OID CMD
	DATATYPE_OID15,				//OID1.5代数据
	DATATYPE_OID2,				//OID2代数据
	DATATYPE_OID3,				//OID3代数据
	DATATYPE_OIDPOSTION,		//OID position code数据
}OIDDATATYPE;

//
typedef struct OidDataTag{

	OIDDATATYPE OidDataType;			//标识OID码的类型
	u32 		dwOidVaidIndex;			//OID码值
	u32 		X;						//OID码值
	u32 		Y;						//OID码值
	WORD 		wAngle;					//角度

}OIDDATAITEM;


typedef struct tag{
    u32 BookIndex;            	//SDCard_1 or SDCard_2 or XtraROM
	u32 OID2BookIndex;			//File's directory first cluster
	u32 DirIndex;				//The index of entry of file in file's directory
	u32 Res;					//The index of entry of file in file's directory

	u32 Index_Max;
	u32 Index_Min;
	u32 Reserved;
	u32 Reserved_1;

} BOOKINDEXINFO;

//Oid data type
typedef enum OidElementPlayTag
{
	eElementSound_PlayOne = 0,				//点一次按顺序只播放一个,连续点时第一次播放第一个，第二次播放第二个。。。，到最后循环回去
	eElementSound_PlayAll_BySequence, 		//点一次顺序播放所有的（default）
	eElementSound_PlayAll_ByRandom,			//点一次随机播放所有的
	eElementSound_PlayOne_ByRandom			//点一次随机播放其中一个

}TElementSoundPlayFlag;

//
#define SNLOGTAG			"SnLib"

#define SEEK_SET			0
#define FS_HANDLE			DWORD//FIL*//FILE*				  		//
#define FS_FILE_BEGIN 0//SEEK_SET

//#define FS_Read(pFile, Ptemp, size, r) 		    fs_read((unsigned char*)Ptemp,1,size,pFile)			 				  		//
//#define FS_Seek(pFile, t, FS_FILE_BEGIN) 	    fs_seek(pFile,t,FS_FILE_BEGIN)

extern unsigned short g_wPlayStatus;


//常数定义
#define DEC_BNF_MP3_FLAG			1  		//
#define DEC_DIC_MP3_FLAG			2  		//
#define DEC_BNF_MP3_FLAG2			3  		//
#define DEC_DIC_MP3_FLAG2			4  		//

#define SPI_SIZE_POS_IN_FLASH		0x44  	//记在spi中的spi size 位置
#define HDR_RM_VISIBLE_STAMP		88		//WORD OFFSET
#define BOK_VER_STAMP_Len			12		//WORD
#define HDR_RM_OID2_BOKCODE			4		//WORD OFFSET
#define HDR_RM_OID3_BOKCODE			6		//WORD OFFSET

//-----------------------------------------------------------------------------------------------------------------------------------------
//录音gain设置值
#define REC_SET_GAIN_VALUE			0X5D		//
#define REC_SET_SKIP_VALUE			200		//

//-----------------------------------------------------------------------------------------------------------------------------------------
//for 写spi的最后那几个sector
#define WRITE_LAST_SECTOR	 		2	//1000 //1000
#define WRITE_LAST_SECTOR_3	 		3	//1000 //1000

#define POWERON_INDEX_DEMO_MODE 	0	//for select power on entry index demo mode //1 ,entry demo mode
#define SLOW_TIME_OUT_SEC 			5//30
#define SLOW_SPEED 					SLOW_32K//HIGH_6M	//SLOW_1P5M SLOW_6M SLOW_32K	//降速目标

#define WRITETOSPILOOPTYPE_INDEX 	1	//若点读过程要保存，则保存的数据类型1表示为index
#define WRITETOSPILOOPTYPE_KEY 		2	//若点读过程要保存，则保存的数据类型1表示为key

//-----------------------------------------------------------------------------------------------------------------------------------------
//extern WORD g_uiOptStatus;		//对应状态如下
#define OPT_STATUS_NONE					0		//不对应任何
#define OPT_STATUS_BOOK_OPEN			1		//不对应任何
#define OPT_STATUS_BOOK_CHK				2		//不对应任何
#define OPT_STATUS_BOOK_INIT			4		//不对应任何
#define OPT_STATUS_BOOK_FAIL			5		//不对应任何
#define OPT_STATUS_BOOK_SUCESS			6		//不对应任何
#define OPT_STATUS_BOOK_FINISH			7		//不对应任何
#define OPT_STATUS_PWR_DOWN_START		8		//
#define OPT_STATUS_PWR_DOWN_PLAYING		9		//
#define OPT_STATUS_PWR_DOWN_PLAYEND		10		//
#define OPT_STATUS_PWR_DOWN_END			11		//
#define OPT_STATUS_BAT_LOW_START		12		//
#define OPT_STATUS_BAT_LOW_PLAYING		13		//
#define OPT_STATUS_BAT_LOW_PLAYEND		14		//
#define OPT_STATUS_BAT_LOW_END			15		//
#define OPT_STATUS_GAME_PROC			16		//


//Rec & play status值
#define REC_STATUS_NONE					0	//
#define REC_STATUS_STARTSOUND			17	//
#define REC_STATUS_READY				18	//
#define REC_STATUS_RECING				19
#define REC_STATUS_ENDSOUND				20	//
#define REC_STATUS_RECEND				21
#define REC_STATUS_RECPLAY				22
#define REC_STATUS_RECPLAYING			23

//-----------------------------------------------------------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------------------------------------------------------------
//game type define
#define GAME_NONE					0		//不对应任何game内容
#define GAME_RANDOM 				0x0200	//random game
#define GAME_SELECT 				0x0201	//select game
#define GAME_MAZE 					0x0202	//maze game
#define GAME_SPELLING				0x0203	//english spelling
#define GAME_CHINESE_SPELL			0x0204	//中文拼字
#define GAME_DICTIONARY 			0X0205	//英文字典
#define GAME_SINGLE_GROUP 			0X0101	//共用一组提示音游戏
#define GAME_MANY_GROUP 			0X0100	//问题有各自提示音游戏
#define GAME_KEY_OBJECT 			0X0102	//关键物件
#define GAME_RECORD 				0X0103	//录音游戏
#define GAME_MANY_GROUP_REMEM		0X0104	//问题有各自提示音游戏
//转换的游戏类型
#define GAME_CHANGETO_SS_RANDOM		0X0600	//0：游戏类型0，随机提问
#define GAME_CHANGETO_SS_SELECT		0X0601	//1：游戏类型1，点码提问
#define GAME_CHANGETO_SS_MAZE_RANDOM		0X0602	//
#define GAME_CHANGETO_SS_MAZE_SELECT		0X0603	//
#define GAME_QUESTION_RANDOM		0X0604	//随机游戏
#define GAME_QUESTION_RAFIXWRONG	0X0605	//随机固定错误答案游戏
#define GAME_QUESTION_SEL			0X0606	//选择问题答
#define GAME_QUESTION_SEQ			0X0608	//
#define GAME_QUESTION_GRADE			0X0607	//分难度等级游戏，
#define GAME_QUESTION_TIMEOUT		0X0609	//选择随机顺序定时游戏，
#define GAME_LEVEL_RMLQ				0X060E	//等级，随机，迷宫，连线，顺序

//录音游戏控制
#define GAME_DIY_DISABLE 			0
#define GAME_DIY_ENABLE 			1
#define GAME_DIY_PLAYBACK 			2

//-----------------------------------------------------------------------------------------------------------------------------------------
//mode define
#define IDLE_MODE					0	//idle mode,for 等待关机等
#define	MP3_PLAY_MODE				1	//mp3 音乐播放 模式
#define	REC_MODE	    			2	//录音模式
#define	REC_PLAY_MODE				3	//录音回放模式
#define USB_MODE					4	//USB模式
#define OID_PLAY_MODE				5	//点读模式
#define TEMP_PLAY_MODE				6	//用于播放开始录音提示音
#define TEMP2_PLAY_MODE				7	//用于播放结束录音提示音
#define GAME_PLAY_MODE  			8	//游戏模式
#define INDEXDEMO_PLAY_MODE  		9 	//用于index demo
#define OID_PLAY_MODE_TEMP			10	//点读模式中转，for some code in this mode,for save pram
#define OID_OPENAPP_MODE			11	//open app模式，for转到用户自加的dlo
#define OID_READAFTER_MODE			12	//跟读模式
#define OID_SOUND_DIY_R_MODE		13	//DIY录音模式
#define OID_SOUND_DIY_P_MODE		14	//DIY录音回放模式
#define MODE_SEL_MODE				15	//模式选择，for有LCD时，通过LCD直选各模式
#define RF_MODE						16	//无线模式，for只需要送出index
#define DIC_MODE					17	//字典模式
#define TEST_MODE					18	//测试模式
#define UPDATE_LIST_MODE			19	//更新book list模式
#define DEBUG_MODE					20	//debug mode
#define LINE_MODE					21	//量产模式

//-----------------------------------------------------------------------------------------------------------------------------------------
#define CARD_BOOK_NO_SEL			0	//没有选择卡片
#define CARD_BOOK_ACTIVE			1	//选择了卡片，当前打开书为卡片
#define CARD_BOOK_CODE				2	//选择了卡片，当前打开书为卡片

//OID书本文件目录路径
#define EXTNAME "bnf"					//文件扩展名
//just for update list
#define OIDFILEDIR_LIST 			"sd:0:\\OID"					//for usb mp,usb out
#define BOOKLISTPATH_LIST 			"sd:0:\\System\\check.bin"		//for usb mp,usb out
extern WORD const OIDFILEDIR[];		//书本目录路径
extern WORD const BOOKLISTPATH[];	//list文件名路径
extern WORD const RECFILEDIR[];		//录音文件目录路径
extern WORD const DICFILEDIR[];		//字典文件目录路径
extern WORD const DIYRECFILEPATH[];		//DIYRec目录路径
extern WORD const RECFOLLOWFILE[];

extern const u32 Tb_AbcSpellGameIndex[];
extern const u32 Tb_AbcRecPlayIndexSound[];
extern const u32 Tb_AbcSpecialAnsAddSound[];
extern const u32 Tb_AbcGameAlwayAns[];
extern const u32 Tb_AbcShuShuGameIndex[];

//变量定义
//OID play要用到的全局量
extern FS_HANDLE pOidPlayFile;					//不与mp3共用，以便在来回切换时，还在原来的文件上
extern WORD g_uiPtemp2[256];		//读取数据中转buffer.因FAT提供的读sector只能从一个sector的开始读数据，如不是sector读出数据是不对的
//用于保存读出的一个sector数据
extern WORD g_uiPtemp[MAX_TEMP_BUF_LEN]; 		//与parsing index共用一个buffer,不知此处理文件的API速度如何，是否跟得上
extern WORD g_wScenarioActive;			//当前scenario号，从0开始
extern WORD g_wScenarioCnt;			//当前书本的scenario总数
extern WORD g_iNeedPlaySoundCnt;		//index需要播放的声音个数
extern WORD g_iOidPlayActiveSoundNo; 	//当前正在播放index 的第几个声音
extern u32 g_ulIndexCount;				//当前书本的 index 总数
extern WORD g_uiBookValidFlag;			//
extern WORD g_wEditID;					//标识程序的Edit ID;由代理程序设置。可以省掉qin
extern u32 g_dwMinIndex; 				// min index
extern u32 g_dwMaxIndex; 				// max index

extern u32 g_u32IndexSoundGroup[INDEX_MULTI_SOUND_MAX];	//multi sound 声音组
extern WORD g_GameType;					//游戏类型
extern WORD g_uiCurrentActiveMode;		//当前的mode

extern WORD g_uiSelCardBook;			//是否选择了卡片标识，
extern u32 g_dwActiveBookCode; 		//实际打开的book code值，单个或是组合的经类型解码后均用此值表示，仅书码
extern u32 g_dwPreBookCode; 			//当前打开的书本的书码
extern WORD g_wIndexInfoSize; 			//index info size
extern u32 g_ulCheckBookTemp; 		// temp use，不能作其它用
//random data
extern u32 g_u32RandomValue;		 	//系统随机数
extern u32 g_kTemp[4]; 				// index info temp

extern WORD g_uiRepeatTime;				//
extern WORD g_uiBookInfoStart;
extern WORD g_uiSecInfoStart;			//
extern WORD g_uiIndexRangeStart;		//

extern WORD g_uiGameDiyEnable;				//
extern u32 g_dwPrePhycialIndex;			//上一个码
extern u32 g_ulIdleCodeSoundNum;			//idle code sound number
#if ENABLE_SERIAL_INDEX_CHG_SCE
extern WORD g_wSecnarioPreActive;		//标识系统当前的scenario
#endif
extern WORD g_SysClock;					// 2012.06.15
extern WORD g_wRise;					//升频标识

extern u32 g_u32OpenAppBinAddress;	//
extern WORD g_dwActiveWriteAddr;		//
extern WORD g_uiIndexRangeArray[QUESTION_ARRAY_SIZE]; //问题数组

extern u32 g_u32PreReadSector;						//
extern FS_HANDLE g_pPreFile;						//上一次调用读取funcion的文件handle
extern WORD g_wPreDecType;							//

//
extern	char g_cFilePath[MAX_PATH_CNT];	//[67];	 	//记录文件路径，以便打开.//buffer不能太大，要不然会出错,文件名长度不能超过此，否则出错，

extern WORD g_uiOptStatus;			//
extern WORD g_wPreAnsQuesNum;			//
extern WORD g_wPrevPlaySceSoundNum;			//
extern WORD g_wSceSoundActNum;			//
extern u32 g_ulPreAnsIndex;			//
extern WORD g_wFileVer;				//
extern u32 g_dwBookRTemp[3];		//
//change for layer
extern WORD g_wLayerCnt;				//
extern WORD g_wLayerActive;				//
extern u32 g_dwSoundNumTemp;			//

extern u32 g_dwMp3StartAddr;				//
extern WORD g_wLibDebugOutputCtrl;				//

extern WORD g_wLibDebugOutputCtrl;				//
extern WORD g_wRWDebug;	//
extern TElementSoundPlayFlag m_elementPlayFlag;
extern DWORD m_dwPreElement;				//
extern WORD m_wPreSoundNum;				//

//函数定义

#define SetReReadFile() g_u32PreReadSector = 0xffffffff
//-----------------------------------------------------------------------------------------------------------------------------------------
//Function:
//	OidStopAllPlaying
//Description:
//	停止所有当前声音播放动作及未完成播放的动作
//Input:
//
//OutPut:
//
//Return:
//
//Others:
//
//-----------------------------------------------------------------------------------------------------------------------------------------
void OidStopAllPlaying(void);


//-----------------------------------------------------------------------------------------------------------------------------------------
//Function:
//	OpenBookFile
//Description:
//	按书码检查书本并打开书本，传入实际的书码值，及设定的edit ID,如能正常打开，能将文件指针给pFile,
//	返回结果检查及打开的状况。
//	如果书码无对应书本内容，并不会覆盖之前打开的书本。仍停留在之前的书本中，
//	在书本文件被使用前，必需用此function作检查，并打开书本，方能获取正确的书本内容。
//Input:
//	WORD BookIndex  : 传入的书码值，实际的index值
//	WORD uIndexTpye :当前点到书码的index 类型。
//	WORD wEditID	: edit id(书本的,标识FW的edit id)
//OutPut:
//	FS_HANDLE *pFile			: 用于接收打开书本文件的句柄（file handle）
//	//file min/maxIndex等相关信息
//Return:
//  0 ：打开书本成功
//  1 ：TF卡没有数据，
//  2 ：打开文件不成功
//	3 ：TF卡中没有此书码的书
//Others:
//
//-----------------------------------------------------------------------------------------------------------------------------------------
//WORD OpenBookFile(u32 BookIndex,WORD uIndexTpye,FS_HANDLE *pFile,WORD wEditID);

//-----------------------------------------------------------------------------------------------------------------------------------------
//Function:
//  CheckBookFile
//Description:
//  检查书本有效性，如有效，同时作初始化书本
//Input:
//  FS_HANDLE pFile         : 用于接收打开书本文件的句柄（file handle）
//OutPut:
//  //file min/maxIndex等相关信息
//Return:
//  0 ：有效书
//  非0 ：无效书
//Others:
//  请确保至少还可以打开一个文件，以及一个文件夹
//-----------------------------------------------------------------------------------------------------------------------------------------
WORD CheckBookFile(FS_HANDLE pFile);

//-----------------------------------------------------------------------------------------------------------------------------------------
//Function:
//  InitBookFile
//Description:
//  检查书本有效性，如有效，同时作初始化书本
//Input:
//  FS_HANDLE pFile         : 用于接收打开书本文件的句柄（file handle）
//OutPut:
//  //file min/maxIndex等相关信息
//Return:
//  0 ：无效书
//  1 ：有效书
//Others:
//  请确保至少还可以打开一个文件，以及一个文件夹
//-----------------------------------------------------------------------------------------------------------------------------------------
WORD InitBookFile(FS_HANDLE pFile);

//-----------------------------------------------------------------------------------------------------------------------------------------
//Function:
//	GetIndexSoundGroup
//Description:
//	获取index要播放的声音组编号，并写入全局数组中，不作为参数传递为省空间
//Input:
//	FS_HANDLE pFile					：书本文件的handle
//	DWORD u32Index					: index 值
//OutPut:
//	全局参数：
//	g_u32IndexSoundGroup			：声音编号保存于此数据中
//	g_iNeedPlaySoundCnt;			//需要播放的声音个数
//	g_iOidPlayActiveSoundNo;		//=0，当前正在播放index 的第几个声音，从0号开始播放
//Return:
// 	0 ：书本无效，或是index没有定义声音
// 	1 ：获取声音号成功
//Others:
//	点到码且确认为element后方可取声音组
//-----------------------------------------------------------------------------------------------------------------------------------------
WORD GetIndexSoundGroup(FS_HANDLE pFile,DWORD u32Index);


//-----------------------------------------------------------------------------------------------------------------------------------------
//Function:
//	GetSoundInfo
//Description:
//	获取声音编号对应的声音信息，开始地址及结束地址
//Input:
//	FS_HANDLE pFile						：书本文件的handle
//	u32 u32SoundNumber		: 要播放的声音号
//OutPut:
//	u32 *dwMp3PlayStartAddr	: 用于保存声音在文件中的开始位置
//	u32 *dwMp3PlayEndAddr		：用于保存声音在文件中的结束位置
//Return:
// 0 ：书本无效，或是声音号超范围
// 1 ：获取声音信息成功
//Others:
//
//-----------------------------------------------------------------------------------------------------------------------------------------
WORD GetSoundInfo(FS_HANDLE pFile,u32 u32SoundNumber,u32 *dwMp3PlayStartAddr,u32 *dwMp3PlayEndAddr);


//-----------------------------------------------------------------------------------------------------------------------------------------
//Function:
//  GetPlayAllSoundCount
//Description:
//	取得书本要连续播放的总数
//Input:
//	FS_FILE *pFile						：书本文件的handle
//OutPut:
//	WORD *SoundCount			: 保存要播放的声音总个数
//Return:
//  0	：取值失败
//	1	：取值成功
//Others:
//
//-----------------------------------------------------------------------------------------------------------------------------------------
WORD GetPlayAllSoundCount(FS_HANDLE pFile,WORD *SoundCount);

//-----------------------------------------------------------------------------------------------------------------------------------------
//Function:
//  GetPlayAllSoundNoGroup
//Description:
//	获取playall的声音号，一次取buffer size大小的声音号，播放后再取，直到结束，所以lib外要判断，处理
//Input:
//	FS_FILE *pFile						：书本文件的handle
//	u32 StartSoundNumber		: 要从第几个声音开始播放
//OutPut:
//  g_u32IndexSoundGroup            ：声音编号保存于此数据中
//  g_iNeedPlaySoundCnt;            //需要播放的声音个数
//  g_iOidPlayActiveSoundNo;        //=0，当前正在播放index 的第几个声音，从0号开始播放
//Return:
//  0	：取值失败
//	1	：取值成功
//Others:
//
//-----------------------------------------------------------------------------------------------------------------------------------------
WORD GetPlayAllSoundNoGroup(FS_HANDLE pFile,u32 StartSoundNumber);




//-----------------------------------------------------------------------------------------------------------------------------------------
//Function:
//	GetIdleSoundGroup
//Description:
//	获取 idle 码所要播放的声音组编号，
//Input:
//
//OutPut:
//	全局参数：
//	g_u32IndexSoundGroup			：声音编号保存于此数据中
//	g_iNeedPlaySoundCnt;			//需要播放的声音个数
//	g_iOidPlayActiveSoundNo;		//=0，当前正在播放index 的第几个声音，从0号开始播放
//Return:
// 	0 ：书本无效，或是index没有定义声音
// 	1 ：获取声音号成功
//Others:
//
//-----------------------------------------------------------------------------------------------------------------------------------------
WORD GetIdleSoundGroup(FS_HANDLE pFile);

//#if(!USE_AIDEDDLO)
//-----------------------------------------------------------------------------------------------------------------------------------------
//Function:
//	GetScenarioSoundGroup
//Description:
//	获取 scenario 码所要播放的声音组编号，
//Input:
//	FS_HANDLE pFile					：书本文件的handle
//	WORD LogicScenarioNo	：要取得的Scenario编号，从0开始
//OutPut:
//	全局参数：
//	g_u32IndexSoundGroup			：声音编号保存于此数据中
//	g_iNeedPlaySoundCnt;			//需要播放的声音个数
//	g_iOidPlayActiveSoundNo;		//=0，当前正在播放index 的第几个声音，从0号开始播放
//Return:
// 	0 ：书本无效，或是index没有定义声音
// 	1 ：获取声音号成功
//Others:
//
//-----------------------------------------------------------------------------------------------------------------------------------------
WORD GetScenarioSoundGroup(FS_HANDLE pFile,WORD LogicScenarioNo);


//-----------------------------------------------------------------------------------------------------------------------------------------
//Function:
//	GetBookSoundGroup
//Description:
//	获取书码所要播放的声音组编号，
//Input:
//	FS_HANDLE pFile					：书本文件的handle
//OutPut:
//	全局参数：
//	g_u32IndexSoundGroup			：声音编号保存于此数据中
//	g_iNeedPlaySoundCnt;			//需要播放的声音个数
//	g_iOidPlayActiveSoundNo;		//=0，当前正在播放index 的第几个声音，从0号开始播放
//Return:
// 	0 ：书本无效，或是index没有定义声音
// 	1 ：获取声音号成功
//Others:
//
//-----------------------------------------------------------------------------------------------------------------------------------------
WORD GetBookSoundGroup(FS_HANDLE pFile);


//-----------------------------------------------------------------------------------------------------------------------------------------
//Function:
//	SetScenario
//Description:
//	获取书码所要播放的声音组编号，
//Input:
//	WORD LogicScenarioNo	：要设定的secnario 的logic值.第几个scenario
//OutPut:
//
//Return:
//
//Others:
//
//-----------------------------------------------------------------------------------------------------------------------------------------
void SetScenario(WORD LogicScenarioNo);
//#endif

void SetLayer(WORD wLogicLayer);

//-----------------------------------------------------------------------------------------------------------------------------------------
//Function:
//  GetLayerSoundGroup
//Description:
//  获取 Layer 码所要播放的声音组编号，
//Input:
//  FS_HANDLE pFile                  ：书本文件的handle
//  WORD wLogicLayerNum    			 ：要取得的Layer编号，从0开始
//OutPut:
//  全局参数：
//  g_u32IndexSoundGroup            ：声音编号保存于此数据中
//  g_iNeedPlaySoundCnt;            //需要播放的声音个数
//  g_iOidPlayActiveSoundNo;        //=0，当前正在播放index 的第几个声音，从0号开始播放
//Return:
//  0 ：书本无效，或是index没有定义声音
//  1 ：获取声音号成功
//Others:
//
//-----------------------------------------------------------------------------------------------------------------------------------------
WORD GetLayerSoundGroup(FS_HANDLE pFile, WORD wLogicLayerNum);

//game 相关API
//-----------------------------------------------------------------------------------------------------------------------------------------
//Function:
//	GetGameType
//Description:
//	获取index在书本中对应的game 类型，
//Input:
//	FS_HANDLE pFile : 要查找的书本文件handle
//OutPut:
//
//Return:
//	0 : 不是gameIndex
//	非0 ：对应的GameType
//Others:
//	在确定认为game index后调用此function才会有效
//-----------------------------------------------------------------------------------------------------------------------------------------
WORD GetGameType(FS_HANDLE pFile);

//-----------------------------------------------------------------------------------------------------------------------------------------
//Function: AfterMP3FillBuffer
//Description: After MP3 read sector data from SD Card
//Input:    pBuffer 		-> 	Buffer Addr pointer. (Data Length is 512 Bytes)
//			dwSoundDataAddr ->	need dec data's addr
//Return:   None
//Others:   when read data,need call this function to dec sound data,
//			one time is 512B,
//-----------------------------------------------------------------------------------------------------------------------------------------
void AfterMP3FillBuffer(u16* pBuffer,u32 dwSoundDataAddr);



//检查是否是无效芯片
//0：不是
//1：是
//-----------------------------------------------------------------------------------------------------------------------------------------
//Function:
//	IsInvalidChip
//Description:
//	检查是否是无效芯片
//Input:
//
//OutPut:
//
//Return:
//	0：不是
//	1：是
//Others:
//
//-----------------------------------------------------------------------------------------------------------------------------------------
WORD IsInvalidChip(void);



//-----------------------------------------------------------------------------------------------------------------------------------------
//Function:
//	GetBookPhyIndex
//Description:
//	读取书本对应的书本码值
//Input:
//  FS_HANDLE pFile         		: 所取书本文件的句柄（file handle）
//OutPut:
//	u32 *pdwOID3BookCode :用于接收返回的OID3的书本码值
//	u32 *pdwOID2BookCode :用于接收返回的OID2的书本码值
//Return:
//  no
//Others:
//
//-----------------------------------------------------------------------------------------------------------------------------------------
void GetBookPhyIndex(FS_HANDLE pFile, u32 *pdwOID3BookCode, u32 *pdwOID2BookCode);

u32 MakeDword(WORD *pwTemp);

//-----------------------------------------------------------------------------------------------------------------------------------------
//Function:
//	GetCardBookFirstIndexRange
//Description:
//	读取书本对应的第一个卡片码范围，点到此范围的码，可以直接进入此书本，而不需点书码
//Input:
//  FS_HANDLE pFile         		: 所取书本文件的句柄（file handle）
//OutPut:
//	u32 *pdwRangeStart 	:码值范围的开始值
//	u32 *pdwRangeEnd 		:码值范围的结束值
//Return:
//  0	：无对应范围
// 非0	：用于取得下一个范围的pos
//Others:
//
//-----------------------------------------------------------------------------------------------------------------------------------------
WORD GetCardBookFirstIndexRange(FS_HANDLE pFile,u32 *pdwRangeStart, u32 *pdwRangeEnd);

//-----------------------------------------------------------------------------------------------------------------------------------------
//Function:
//	GetCardBookNextIndexRange
//Description:
//	读取书本对应的下一个卡片码范围，点到此范围的码，可以直接进入此书本，而不需点书码
//Input:
//  FS_HANDLE pFile         		: 所取书本文件的句柄（file handle）
//	WORD wPos						：GetCardBookFirstIndexRange或GetCardBookNextIndexRange返回的非0值
//OutPut:
//	u32 *pdwRangeStart 	:码值范围的开始值
//	u32 *pdwRangeEnd 		:码值范围的结束值
//Return:
//  0	：无对应范围
// 非0	：用于取得下一个范围的pos
//Others:
//
//-----------------------------------------------------------------------------------------------------------------------------------------
WORD GetCardBookNextIndexRange(FS_HANDLE pFile,WORD wPos,u32 *pdwRangeStart, u32 *pdwRangeEnd);

//call back function
 WORD FS_GetFilePosition(FS_HANDLE pFile,u32 *dwPreFilePos);
 extern u32 FS_Seek(FS_HANDLE pFile,u32 dwSeekAddr,u32 dwSeekWay);
 extern u32 FS_Read(FS_HANDLE pFile,BYTE *pbReadBuf, u32 dwReadLen,u32 *dwBr);

//-----------------------------------------------------------------------------------------------------------------------------------------
//Function:
//  OID_GetOidData
//Description:
//  获取相应点读数据
//Input:
// u32 dwOidData1						收到two wire 的低4byte
// u32 dwOidData2						收到two wire 的高4byte,注意接收two wire时是MSb to LSb serially
//OutPut:
// OIDDATAITEM *pOidDataItem:			接收读取的数据buffer
//Return:
// 0	:获取成功
// 非0	：无效数据
//Others:
//
//-----------------------------------------------------------------------------------------------------------------------------------------
WORD OID_GetOidValueData(OIDDATAITEM *pOidDataItem,u32 dwOidData1,u32 dwOidData2);

WORD MP3_GetStatus(void);

//-----------------------------------------------------------------------------------------------------------------------------------------
//Function:
//  GetLastIndexSoundGroup
//Description:
//  取得最后点的一个码(除内容码外)，所添加的声音，如明有添加声音的话
//Input:
//  FS_HANDLE pFile                  ：书本文件的handle
//OutPut:
//  全局参数：
//  g_u32IndexSoundGroup            ：声音编号保存于此数据中
//  g_iNeedPlaySoundCnt;            //需要播放的声音个数
//  g_iOidPlayActiveSoundNo;        //=0，当前正在播放index 的第几个声音，从0号开始播放
//Return:
//  0 ：书本无效，或是index没有定义声音
//  1 ：获取声音号成功
//Others:
//  点到码且确认为element后方可取声音组
//-----------------------------------------------------------------------------------------------------------------------------------------
u16 GetLastIndexSoundGroup(FS_HANDLE pFile);

extern DWORD FS_GetFileLen(FS_HANDLE pFile);



//-----------------------------------------------------------------------------------------------------------------------------------------
//Function:
//  GetScenarioTranslateSecNo
//Description:
//	获取scenario下的index 翻译内容所在的scenario号,
//Input:
//	FS_FILE *pFile					：书本文件的handle
//	u16 LogicScenarioNo				: 要取翻译号的scenario number
//OutPut:
//
//Return:
// 	0 	：书本无效，或是没有设定翻译内容
// 	非0	：返回值即为(翻译的scenario号+1)，
//Others:
//	scenario号是从0算起的，按每个layer下面顺序往下排，实际要用翻译的scenario号时，要-1
//假设返回的值为A,则要先确定A属于那个layer下面的那个scenario,如下
//LayerNum 		= (A - 1) / ScenarioCnt; 	(ScenarioCnt 可以直接用定义的变量g_wScenarioCnt)
//ScenarioNum 	= (A - 1) % ScenarioCnt;
//将此LayerNum,ScenarioNum做相应的setLayer,setScenario,然后调用GetIndexType才能获得相应的类型及内容
//setLayer,setScenario也可以直直接改变相应定义的变量
//setLayer() 	== (g_wLayerActive 		= LayerNum)
//ScenarioNum() == (g_wScenarioActive 	= ScenarioNum)
//-----------------------------------------------------------------------------------------------------------------------------------------
u16 GetScenarioTranslateSecNo(FS_HANDLE pFile, u16 LogicScenarioNo);

//-----------------------------------------------------------------------------------------------------------------------------------------
//Function:
//  GetElementIdxTransSoundGroup
//Description:
//	获取scenario下的index 翻译内容的声音,
//Input:
//	FS_FILE *pFile					：书本文件的handle
//	DWORD dwIndex					: 要获取翻译声音的码值
//OutPut:
//
//Return:
// 	0 	：书本无效，或是没有设定翻译内容
// 	1	：获取翻译内容成功
//Others:
//-----------------------------------------------------------------------------------------------------------------------------------------
WORD GetElementIdxTransSoundGroup(FS_HANDLE pFile, DWORD dwIndex);

#endif//#ifndef __OID_LIB__
