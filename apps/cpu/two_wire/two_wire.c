#include "uart.h"
#include "two_wire.h"
#include "sdk_cfg.h"

/*

master ---- DSP/MCU
slave ----- OID decoder

*/

/*
Default : SCK is kept low by Master, and SDIO is released and pulled high by
external pull-up resister.
*/

#define DEBOUNCE_TIMES 3



void master_init()
{
    tw_clk_out();
    tw_clk_l();

    tw_data_in(); //ready to rec data
    tw_delay();
}


/*
Master initiates a transfer cycle by changing SCK from low to high.
*/
void master_send_start()
{
    tw_clk_l();
    tw_delay();
    tw_clk_h();
    tw_delay();
}


/*
If Master keeps low on SCK over 78¦Ìs.
Slave supposes that the transfer cycle is finished.
*/
void master_send_end()
{
    tw_clk_out();
    tw_data_in();
    tw_clk_l();
}


/*
Before Read cycle, Slave generates transfer request (pulling low SDIO line) to inform
Master.
*/
bool master_check_revstate()
{
    tw_data_in();
    //delay_n10ms(1);
    //delay_n10ms(5);
    delay_2ms(1);
    if(tw_data_r())//no receive date
        return FALSE;
    else
        return TRUE;
}


u16 master_wait_ack()
{
	u16 i = 0, receive_dat = 0, count = 0;

    if(master_check_revstate() == FALSE)//no ACK
        return receive_dat;

    master_send_start();

    tw_data_out(); // read control bit
    tw_delay();
    tw_data_l();
    tw_delay();

    tw_clk_l();    //positive edge
    tw_delay();
    tw_clk_h();
    tw_delay();

    tw_data_in();
    tw_delay();

	for(i=0; i<16; i++)
	{
        tw_clk_l();
        tw_delay();

        receive_dat <<= 1;
        if(tw_data_r())
            receive_dat |= 0x01;

        tw_clk_h();
        tw_delay();
	}

    master_send_end();

//    log_printf("wait ACK : 0x%x\n", receive_dat);
	return receive_dat;
}



void master_send_onebit(bit1 bit)
{
    tw_data_out();
    tw_delay();

    if(bit)
        tw_data_h();
    else
        tw_data_l();

    tw_clk_h();
    tw_delay();
    tw_clk_l();
    tw_delay();
    tw_clk_h();
    tw_delay();
}


void master_send_onebyte(u8 data)
{
    log_printf("master_send_one byte start:0x%02x\n", data);

    u8 temp = data, count = 0;

    while(count++ <10)//SDIO should be checked before every Write cycle.
    {
        if(master_check_revstate() == FALSE)
            break;
        else
        {
            master_send_end(); //end signal
            log_printf("stop read.\n");
        }
    }

    if(count==10) //not write.
        return;

    log_printf("master_send_one byte go go go ....\n");

    master_send_start(); //start signal

    master_send_onebit(1);//write control bit 1

    for(int i=0; i<8; i++)                //write one byte
    {
        if((data & 0x80)>0)               //MSB bit
           master_send_onebit(1);         //pull high data IO
        else
           master_send_onebit(0);         //pull low data IO

        data <<= 1;
    }

    master_send_end();                    //end signal

    count = 0;
    while(count++ < 20)
    {
        if(master_wait_ack() == ((u16)temp+1) )    //check send is valid.
        {
            log_printf("send valid.\n");
            break;
        }
        else
        {
            log_printf("send invalid, count: %d\n", count);
            tw_delay();
        }
    }

    log_printf("master_send_one byte  end.............\n");
    return  ;
}

/*
Multi-Write Command
*/
void master_send_nbyte(u8 *cmd, u8 len)
{
    if(!cmd || len == 0)
        return ;

    while (len--)
    {
        master_send_onebyte(*cmd++);
        delay_n10ms(30);//between two commands must > 250ms
    }

    return ;
}


u8 master_readfrom_slave_onebyte()
{
    u8 i, receive_dat = 0;

	for(i=0; i<8; i++)
	{
        tw_clk_l();
        tw_delay();

        receive_dat <<= 1;
        if(tw_data_r())
            receive_dat |= 0x01;

        tw_clk_h();
        tw_delay();
	}


	//log_printf("receive_dat:0x%02x\n", receive_dat);
    return receive_dat;
}


void master_readfrom_slave_nbyte(u8 data[], u8 n)
{
    if(!data || n <= 0)
        return ;
    memset(data, 0, n);
    master_send_start(); //start signal

    tw_data_out(); // read control bit
    tw_delay();
    tw_data_l();
    tw_delay();

    tw_clk_l();    //positive edge
    tw_delay();
    tw_clk_h();
    tw_delay();

    tw_data_in();  //released SDIO
    tw_delay();
    while(n--)
    {
       *data = master_readfrom_slave_onebyte();

        data++;
    }

    master_send_end();

    return ;
}




bool master_readfrom_slave(u8 *data)
{
    u8 cnt_time= 0;
    u8 packet_same_times = 0;
    u8 data_temp[8]={0};

    if(!master_check_revstate())//check rev state
        return FALSE;
    #if 1
    {
        while(cnt_time++<250)
        {
            if(!master_check_revstate())//check rev state
                continue;
            master_readfrom_slave_nbyte(data,8);

            if(!(data[0]&BIT(4)))//valid
            {
                if(0==memcmp(data_temp, data, 8)){
                    packet_same_times++;
                }
                else
                {
                    memset(data_temp, 0, 8);
                    memcpy(data_temp,data, 8);
                }
            }
            else//invalid
            {
                continue;
            }

            if(packet_same_times == DEBOUNCE_TIMES)
                break;

        }

        if(cnt_time>250){
            return FALSE;
        }

    }
    #else
    {
        master_readfrom_slave_nbyte(data,8);
    }
    #endif // 1

    log_printf("recivdata: 0x%02x, 0x%02x, 0x%02x, 0x%02x, 0x%02x, 0x%02x, 0x%02x, 0x%02x\n",data[0],data[1],data[2],data[3],data[4],data[5],data[6],data[7]);

    return TRUE;
}



// Setup decoder SOC from DSP/MCU
void master_setup_slave_init()
{
    u8 data[8] = {0}, count= 0;
    u8 init[2] = {0x25,0x40};
    //exit configuration mode end enter to normal mode
    tw_clk_out();
    tw_clk_h();
    delay_n10ms(5);//over 20ms and less than 2sec

    master_init(); //Default state

    while(count < 10)//check Decoder SOC is already entered to normal mode.
    {
        if(master_check_revstate())//check rev state
        {
            memset(data, 0, sizeof(data));
            master_readfrom_slave_nbyte(data,8);
            log_printf("master_readfrom_slave_nbyte:0x%02x,0x%02x,0x%02x,0x%02x,0x%02x,0x%02x,0x%02x,0x%02x\n",data[0],data[1],data[2],data[3],data[4],data[5],data[6],data[7]);

            if(data[6]==0xFF && data[7]==0xF8)
                break;
            else if(data[6]==0xFF && data[7]==0xF6)//check oidpen head.
            {
                tw_data_in();
                tw_clk_in();
                break;
            }
        }
        delay_n10ms(1);
        count++;
    }

    if(count == 10)
    {
        log_printf("\n\n\n\n\n\n\n---------------init two wire fail------------------\n\n\n\n\n\n");
    }
    else
    {
        log_printf("Decoder SOC has already entered to normal mode.\n\n\n");
        master_send_nbyte(init, 2);
    }

}

