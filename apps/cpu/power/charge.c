#include "charge.h"
#include "power.h"
#include "sdk_cfg.h"
#include "rtc/rtc_api.h"
#include "led/led.h"
/* #include "sys_detect.h" */
#include "dac.h"
#include "key_drv/key.h"
#include "audio/dac_api.h"
#include "clock.h"
#include "timer.h"
#include "adc_api.h"
#include "irq_api.h"
#include "clock_api.h"
#include "power_manage_api.h"
#include "warning_tone.h"
#include "task_manager.h"
#include "task_idle.h"
#include "msg.h"
//该宏配置是否开着机充电，当需要保持开机状态充电时，定义该宏
/* #define POWER_ON_CHARGE */





/* #define CHARGEE_DBG */
#ifdef  CHARGEE_DBG
#define charge_putchar        putchar
#define charge_printf         log_printf
#define charge_buf            printf_buf
#else
#define charge_putchar(...)
#define charge_printf(...)
#define charge_buf(...)
#endif    //CHARGEE_DBG








static void delay_ms()
{
    //Timer2 for delay
    JL_TIMER2->CON = BIT(14);
    JL_TIMER2->PRD = 375;
    JL_TIMER2->CNT = 0;
    SFR(JL_TIMER2->CON, 2, 2, 2); //use osc
    SFR(JL_TIMER2->CON, 4, 4, 3); //div64
    SFR(JL_TIMER2->CON, 14, 1, 1); //clr pending
    SFR(JL_TIMER2->CON, 0, 2, 1); //work mode
    while (!(JL_TIMER2->CON & BIT(15)));
    JL_TIMER2->CON = BIT(14);
}

void delay_nms(u32 n)
{
    while (n--) {
        delay_ms();
    }
}


void charge_power_on_detect_deal(void)//开机检测是否插入了充电线
{
    CHE_PIN_IN();
    charge_led_init();

    while (1)
    {
        clear_wdt();
        delay(30000);

        if(!CHE_PIN_STATE())//充电不开机
        {
            log_printf("chargeing not to turn on......\n");
            charge_led_on();
            enter_sys_soft_poweroff();
        }
        else
        {
            log_printf("no chargeing ......\n");
            break;
        }
    }
}





void battery_charging_check(void)//正常工作后,检测充电
{
    static  u32 battery_charging_time = 0;

        if(!CHE_PIN_STATE())
        {
            battery_charging_time++;
            //log_printf("charge ...\n");
            if(battery_charging_time == 2)//2S
            {
                charge_led_on();
                power_led_off();
           //     task_post_msg(NULL, 1, MSG_CHARGE_IN);
            }
        }
        else
        {
           // log_printf("No charge ...\n");
            battery_charging_time = 0;
            charge_led_off();
            power_led_on();
        }



    return ;
}


LOOP_DETECT_REGISTER(charge_detect) = {
    .time = 500,
    .fun  = battery_charging_check,
};
