/*************************************************************************************************************
 * 文件名:			kionix_accel.c
 * 功能:			KXTJ3 Accelerometer驱动
 * 作者:			莫世柄
 * 创建时间:		2020-03-09
 *************************************************************************************************************/


#include "sdk_cfg.h"
#include "typedef.h"
#include "iic.h"
#include "kionix_accel.h"
#include "task_manager.h"

#define KIONIX_DEBUG_ENABLE */

#ifdef KIONIX_DEBUG_ENABLE
#define accel_printf log_printf
#else
#define accel_printf(...)
#define accel_printf(...)
#endif// KIONIX_DEBUG_ENABLE


#if 0
//VDD
#define SAD1   0x0F     //primary
#define SAD2   0x0D     //Flipped
#else
//GND
#define SAD1   0x0E     //primary
#define SAD2   0x0C     //Flipped
#endif

#define KXTJ3_ACCEL_RES_8BIT	0
#define KXTJ3_ACCEL_RES_12BIT	1 //or 14BIT

#define KXTJ3_ACCEL_G_2G		0
#define KXTJ3_ACCEL_G_4G		2
#define KXTJ3_ACCEL_G_8G		4 //6
#define KXTJ3_ACCEL_G_16G       1 //3//5//7


u8 init_ok = 0;
static u8 KXTJ3_SAD = 0xFF;
static u8 KXTJ3_Dirction = 0;

#define I2C_Init   iic_init_io
#define I2C_Start  iic_start
#define I2C_Stop   iic_stop
#define I2C_Send_Byte  iic_sendbyte_io
#define I2C_Wait_Ack   r_ack
#define I2C_RevByte     iic_revbyte


static bool KXTJ3_WriteOneReg(u8 sad_addr, u8 reg_addr, u8 wd_dat)
{
    u8 ret = 0;

    if(sad_addr == 0xFF)
    return -1;

    I2C_Start();

    I2C_Send_Byte(sad_addr << 1);

    if (I2C_Wait_Ack()) {
        accel_printf("ret = 1.\n");
        return 1;
    }

    I2C_Send_Byte(reg_addr);
    if (I2C_Wait_Ack()) {
        accel_printf("ret = 2.\n");
        return 2;
    }

    I2C_Send_Byte(wd_dat);
    if (I2C_Wait_Ack()) {
        accel_printf("ret = 3.\n");
        return 3;
    }

    I2C_Stop();

    return ret;
}

static bool KXTJ3_WriteMultiReg(u8 sad_addr, u8 reg_addr, u8 regnum, u8 wd_dat[])
{
    u8 i;

    if(sad_addr == 0xFF)
    return -1;

    I2C_Start();

    I2C_Send_Byte(sad_addr << 1);

    if (I2C_Wait_Ack()) {
        accel_printf("ret = 1.\n");
        return 1;
    }

    I2C_Send_Byte(reg_addr);
    if (I2C_Wait_Ack()) {
        accel_printf("ret = 2.\n");
        return 2;
    }

    for(i =0; i < regnum; i++)
    {
        I2C_Send_Byte(wd_dat[i]);
        if(I2C_Wait_Ack())
        {
            accel_printf("ret = 3.\n");
            return 3;
        }
    }
    I2C_Stop();

    return 0;
}

static u8 KXTJ3_ReadOneReg(u8 sad_addr, u8 reg_addr)
{
   u8 rev_dat;

   if(sad_addr == 0xFF)
    return 0;

    I2C_Start();

    I2C_Send_Byte(sad_addr << 1);
    if (I2C_Wait_Ack()) {
        accel_printf("ret = 1.\n");
        return 1;
    }

    I2C_Send_Byte(reg_addr);
    if (I2C_Wait_Ack()) {
        accel_printf("ret = 2.\n");
        return 2;
    }

    I2C_Start();

    I2C_Send_Byte((sad_addr << 1)|1);
    if (I2C_Wait_Ack()) {
        accel_printf("ret = 3.\n");
        return 3;
    }

    rev_dat = I2C_RevByte(1);

    I2C_Stop();

    //accel_printf("rev_dat = 0x%02X.\n", rev_dat);
    return rev_dat;
}

static bool KXTJ3_ReadMultiReg(u8 sad_addr, u8 reg_addr, u8 regnum, u8 data[])
{
    u8 i;

    if(sad_addr == 0xFF)
        return -1;

    I2C_Start();

    I2C_Send_Byte(sad_addr << 1);
    if (I2C_Wait_Ack()) {
        accel_printf("ret = 1.\n");
        return 1;
    }

    I2C_Send_Byte(reg_addr);
    if (I2C_Wait_Ack()) {
        accel_printf("ret = 2.\n");
        return 2;
    }

    I2C_Start();

    I2C_Send_Byte((sad_addr << 1)|1);
    if (I2C_Wait_Ack()) {
        accel_printf("ret = 3.\n");
        return 3;
    }

    for(i = 0; i < regnum; i++)
    {
        if(i == regnum-1)
            data[i] = I2C_RevByte(1);
        else
            data[i] = I2C_RevByte(0);
    }

    I2C_Stop();

    return 0;
}

static void KXTJ3_Set_Mode(u8 mode)
{
    u8 data;
    data = KXTJ3_ReadOneReg(KXTJ3_SAD, 0x1B);

    if(mode)
    {
        data |= (1<<7);
    }
    else
    {
        data &= ~(1<<7);
    }
    KXTJ3_WriteOneReg(KXTJ3_SAD, 0x1B, data | (1<<7));
}


void KXTJ3_SELF_TEST(void)
{
    KXTJ3_WriteOneReg(KXTJ3_SAD, 0x3A, 0xCA);
    KXTJ3_WriteOneReg(KXTJ3_SAD, 0x3A, 0x00);
}


static bool KXTJ3_Check_Use_SAD(void)
{
    u8 ret = -1;

    ret = KXTJ3_WriteOneReg(SAD1, 0x7F, 0x00);

    if(ret != 0)
    {
        ret =  KXTJ3_WriteOneReg(SAD2, 0x7F, 0x00);
        if(ret == 0)
        {
            ret = KXTJ3_WriteOneReg(SAD2, 0x1D, 0x00);
            if(ret == 0)
            {
                accel_printf("address use SAD2.\n");
                KXTJ3_SAD = SAD2;
            }
        }
    }
    else
    {

        ret = KXTJ3_WriteOneReg(SAD1, 0x1D, 0x00);
        if(ret == 0)
        {
            accel_printf("address use SAD1.\n");
            KXTJ3_SAD = SAD1;
        }
    }

    return ret;
}


bool KXTJ3_Report_FuncCauseInterrupt(void)  //INT_SOURCE1
{
    u8 data;
    data = KXTJ3_ReadOneReg(KXTJ3_SAD, 0x16);
    accel_printf("INT_SOURCE1:0x%02X.\n", data);
    return data;
}

static void KXTJ3_Report_AxisAndDirection(void) //INT_SOURCE2
{
    u8 data, xn,xp, yn,yp, zn,zp, ret = 0;

    data = KXTJ3_ReadOneReg(KXTJ3_SAD, 0x17);

    xn = data&BIT(5);
    xp = data&BIT(4);
    yn = data&BIT(3);
    yp = data&BIT(2);
    zn = data&BIT(1);
    zp = data&BIT(0);
accel_printf("INT_SOURCE2:%d,%d,%d,%d,%d,%d.\n", xn,xp, yn,yp, zn,zp);

    if((xn||xp) && (yn||yp) && (zn||zp))
    {
        accel_printf("Negative XYX+.\n");
        KXTJ3_Dirction = 1;
    }
    else if(((xn||xp) && (yn||yp))||((xn||xp)&&(zn||zp))||((xn||xp)&&(zn||zp)))
    {
        accel_printf("Negative XY+ || XZ+ || YZ+ Reported.\n");
        KXTJ3_Dirction = 1;
    }
    else if(xn||xp)
    {
        accel_printf("Negative X+ Reported.\n");
        KXTJ3_Dirction = 2;
    }
    else if(yn||yp)
    {
        accel_printf("Negative Y+ Reported.\n");
        KXTJ3_Dirction = 3;
    }
    else if(zn||zp)
    {
        accel_printf("Negative Z+ Reported.\n");
        KXTJ3_Dirction = 4;
    }
}

static u8 KXTJ3_Get_Motion_Dir(void)
{
    u8 dir;
    dir = KXTJ3_Dirction;
    KXTJ3_Dirction = 0;

    return dir;
}

bool KXTJ3_Report_StatueOfInterrupt(void) //STATUS_REG
{
    if(KXTJ3_ReadOneReg(KXTJ3_SAD, 0x18)&BIT(4))//Bit4
        return TRUE;
    else
        return FALSE;
}


void KXTJ3_Clear_InterruptSource(void) //INT_REL
{
    u8 data;
    data = KXTJ3_ReadOneReg(KXTJ3_SAD, 0x1A);
}

void KXTJ3_Poweron_Configure(void)	//初始化配置
{
    KXTJ3_WriteOneReg(KXTJ3_SAD, 0x1B, 0x00);//CTRL_REG1
    KXTJ3_WriteOneReg(KXTJ3_SAD, 0x1D, 0x06);//CTRL_REG2

    KXTJ3_ReadOneReg(KXTJ3_SAD, 0x1A);       //INT_REL

    KXTJ3_WriteOneReg(KXTJ3_SAD, 0x1E, 0x30);//INT_CTRL_REG1
    KXTJ3_WriteOneReg(KXTJ3_SAD, 0x1F, 0x3F);//INT_CTRL_REG2 0xBF

    KXTJ3_WriteOneReg(KXTJ3_SAD, 0x21, 0x02); //DATA_CTRL_REG

    KXTJ3_WriteOneReg(KXTJ3_SAD, 0x29, 1);//WAKEUP_COUNTER
    KXTJ3_WriteOneReg(KXTJ3_SAD, 0x2A, 1);//NA_COUNTER

    KXTJ3_WriteOneReg(KXTJ3_SAD, 0x6A, 0x20);//WAKEUP_THRESHOLD 6A-6B:
    KXTJ3_WriteOneReg(KXTJ3_SAD, 0x6B, 0x00);//WAKEUP_THRESHOLD

    KXTJ3_WriteOneReg(KXTJ3_SAD, 0x1B, 0x92);//CTRL_REG1
}

bool KXTJ3_Poweron_Init(void)	//初始化
{

    I2C_Init();          //init I2C
    INT_data_in();

    I2C_Send_Byte(SAD1); //ADDR pin at GND
    delay_2ms(1);

    I2C_Send_Byte(SAD2); //ADDR pin at GND
    delay_2ms(2);

    if(KXTJ3_Check_Use_SAD() != 0)
    {
       accel_printf("line = %d.\n", __LINE__);
        return FALSE;
    }

    if(KXTJ3_WriteOneReg(KXTJ3_SAD, 0x1D, 0x80) != 0)//CTRL_REG2
    {
       accel_printf("line = %d.\n", __LINE__);
       return FALSE;
    }

    if(KXTJ3_ReadOneReg(KXTJ3_SAD, 0x0F) != 0x35)//WHO_AM_I
    {
       accel_printf("line = %d.\n", __LINE__);
       return FALSE;
    }

    if(KXTJ3_ReadOneReg(KXTJ3_SAD, 0x0C) != 0x55)//DCST_RESP
    {
       accel_printf("line = %d.\n", __LINE__);
       return FALSE;
    }
    accel_printf("KXTJ3_Poweron Check OK.\n");

    KXTJ3_Poweron_Configure();
    accel_printf("KXTJ3_Poweron Config OK.\n");
    init_ok = 1;
     return TRUE;
}



static bool KXTJ3_ReadAcceleration(u16 *Xa, u16 *Ya, u16 *Za)  //报告X,Y,Z三轴加速度
{
    u8 buf[6] = {0};

    KXTJ3_ReadMultiReg(KXTJ3_SAD, 0x06, 6, buf);

//8bit
    *Xa = buf[1];
    *Ya = buf[3];
    *Za = buf[5];
//12bit
//    *Xa = (buf[0]<<4) | (buf[1]>>4);
//    *Ya = (buf[2]<<4) | (buf[3]>>4);
//   *Za = (buf[4]<<4) | (buf[5]>>4);
//    accel_printf("0x%02X,0x%02X,0x%02X,0x%02X,0x%02X,0x%02X\n",buf[0],buf[1],buf[2],buf[3],buf[4],buf[5]);
    accel_printf("*X=%d,*Y=%d,*Z=%d\n",*Xa,*Ya,*Za);

    return TRUE;
}

u8 KXTJ3_poll_thread(void)
{
    return KXTJ3_Get_Motion_Dir();
}

void KXTJ3_Acceleration_detect(void)
{
    u16 x, y, z;
    u8 direction, data;

//if(init_ok!=1)
    if(task_get_cur()!= TASK_ID_BT)
    return ;

    if(INT_data_r())//KXTJ3_Report_StatueOfInterrupt()
    {
       // data = KXTJ3_Report_FuncCauseInterrupt();
       // if(data & BIT(1))
        {
            KXTJ3_Report_AxisAndDirection();
        }

      //  if(data & BIT(4))
        {
       //     KXTJ3_ReadAcceleration(&x, &y, &z);
        }

        KXTJ3_Clear_InterruptSource();

    }

}

#if 1
LOOP_DETECT_REGISTER(Acceleration_detect) = {
    .time = 50,//200,//(2ms)
    .fun  = KXTJ3_Acceleration_detect,
};
#endif
