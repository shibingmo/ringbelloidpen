#ifndef _KIONIX_ACCEL_H_INCLUDED_
#define _KIONIX_ACCEL_H_INCLUDED_

#define INT_PORT        JL_PORTB
#define INT_PIN         BIT(2)

#define INT_data_in()    do{INT_PORT->DIR |=  INT_PIN;INT_PORT->PD |= INT_PIN;}while(0)
#define INT_data_r()     (INT_PORT->IN&INT_PIN)



#endif // _KIONIX_ACCEL_H_INCLUDED_
