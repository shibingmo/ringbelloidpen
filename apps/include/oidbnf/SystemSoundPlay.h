
#ifndef __SYS_H__
#define __SYS_H__

#include "OidIndexSoundFunctionLib.h"
#include "MaxBufDef.h"
#include "SysCtrlBit.h"

//常数定义
//-----------------------------------------------------------------------------------------------------------------------------------------
#define OID_SPEED_LOOP_DEF				0  	//0-分方向，1-循环切换。决定调速是循环调还是单向分两个，以节省代码空间
#define OID_VOLUME_LOOP_DEF				0  	//0-分方向，1-循环切换。决定是循环调还是单向分两个，以节省代码空间
#define OID_FAST_BACK_OR_FOR			0  	//0-no，1-yes。决定是否要快进快退调整功能


extern WORD const SYSTEMFILEPATH[];	//系统文件路径
extern WORD const SYSFILEDIR[];		//系统文件目录路径
//
extern const u32 Tbl_VoiceList[];
extern const u32 Tbl_VoiceListDebug[];

//-----------------------------------------------------------------------------------------------------------------------------------------
//system中，各系统声音编号，要播放那个系统声音用此声音编号去取得声音的开始及结束位置即可播放。
#define SYS_VOICE_POWERON 		0						//开机声音
#define SYS_VOICE_POWEROFF		SYS_VOICE_POWERON + 1	//关机声音
#define SYS_VOICE_SELBOOK		SYS_VOICE_POWEROFF+ 1	//先选书码提示音编号
#define SYS_VOICE_NOTANYBOOK 	SYS_VOICE_SELBOOK+ 1	//系统内无书提示音编号
#define SYS_VOICE_LOWBATTRY  	SYS_VOICE_NOTANYBOOK+ 1 //低电压提示音编号
#define SYS_VOICE_0  			SYS_VOICE_LOWBATTRY+ 1 	//系统数字提示音编号
#define SYS_VOICE_1  			SYS_VOICE_0+ 1 			//系统数字提示音编号
#define SYS_VOICE_2  			SYS_VOICE_1+ 1 			//系统数字提示音编号
#define SYS_VOICE_3  			SYS_VOICE_2+ 1 			//系统数字提示音编号
#define SYS_VOICE_4  			SYS_VOICE_3+ 1 			//系统数字提示音编号
#define SYS_VOICE_5  			SYS_VOICE_4+ 1 			//系统数字提示音编号
#define SYS_VOICE_6  			SYS_VOICE_5+ 1 			//系统数字提示音编号
#define SYS_VOICE_7  			SYS_VOICE_6+ 1 			//系统数字提示音编号
#define SYS_VOICE_8  			SYS_VOICE_7+ 1 			//系统数字提示音编号
#define SYS_VOICE_9  			SYS_VOICE_8+ 1 			//系统数字提示音编号
#define SYS_VOICE_OPENDEMO		SYS_VOICE_9+ 1 			//开启demo的声音提示音编号
#define SYS_VOICE_EXITDEMO		SYS_VOICE_OPENDEMO+ 1 	//退出demo的声音提示音编号
#define SYS_VOICE_BOOK			SYS_VOICE_EXITDEMO+ 1 	//Book.mp3提示音编号
#define SYS_VOICE_IDLE			SYS_VOICE_BOOK+ 1 		//Idle.mp3提示音编号
#define SYS_VOICE_UPDATE_START	SYS_VOICE_IDLE		 	//Book.mp3提示音编号
#define SYS_VOICE_UPDATE_END	SYS_VOICE_BOOK		 	//Idle.mp3提示音编号
#define SYS_VOICE_VOLUMEDOWN 	SYS_VOICE_IDLE+ 1		 //声音调小提示音编号
#define SYS_VOICE_VOLUMEUP		SYS_VOICE_VOLUMEDOWN+ 1  //声音调大提示音编号
#define SYS_VOICE_A  			SYS_VOICE_VOLUMEUP+ 1    //系统字母提示音编号
#define SYS_VOICE_B  			SYS_VOICE_A+ 1		 	 //系统字母提示音编号
#define SYS_VOICE_C  			SYS_VOICE_B+ 1		 	 //系统字母提示音编号
#define SYS_VOICE_D  			SYS_VOICE_C+ 1		 	 //系统字母提示音编号
#define SYS_VOICE_E  			SYS_VOICE_D+ 1		 	 //系统字母提示音编号
#define SYS_VOICE_F  			SYS_VOICE_E+ 1		 	 //系统字母提示音编号
#define SYS_VOICE_G  			SYS_VOICE_F+ 1		 	 //系统字母提示音编号
#define SYS_VOICE_H  			SYS_VOICE_G+ 1		 	 //系统字母提示音编号
#define SYS_VOICE_I  			SYS_VOICE_H+ 1		 	 //系统字母提示音编号
#define SYS_VOICE_J  			SYS_VOICE_I+ 1		 	 //系统字母提示音编号
#define SYS_VOICE_K  			SYS_VOICE_J+ 1		 	 //系统字母提示音编号
#define SYS_VOICE_L  			SYS_VOICE_K+ 1		 	 //系统字母提示音编号
#define SYS_VOICE_M  			SYS_VOICE_L+ 1		 	 //系统字母提示音编号
#define SYS_VOICE_N  			SYS_VOICE_M+ 1		 	 //系统字母提示音编号
#define SYS_VOICE_O  			SYS_VOICE_N+ 1		 	 //系统字母提示音编号
#define SYS_VOICE_P  			SYS_VOICE_O+ 1		 	 //系统字母提示音编号
#define SYS_VOICE_Q  			SYS_VOICE_P+ 1		 	 //系统字母提示音编号
#define SYS_VOICE_R  			SYS_VOICE_Q+ 1		 	 //系统字母提示音编号
#define SYS_VOICE_S  			SYS_VOICE_R+ 1		 	 //系统字母提示音编号
#define SYS_VOICE_T  			SYS_VOICE_S+ 1		 	 //系统字母提示音编号
#define SYS_VOICE_U  			SYS_VOICE_T+ 1		 	 //系统字母提示音编号
#define SYS_VOICE_V  			SYS_VOICE_U+ 1		 	 //系统字母提示音编号
#define SYS_VOICE_W  			SYS_VOICE_V+ 1		 	 //系统字母提示音编号
#define SYS_VOICE_X  			SYS_VOICE_W+ 1		 	 //系统字母提示音编号
#define SYS_VOICE_Y  			SYS_VOICE_X+ 1		 	 //系统字母提示音编号
#define SYS_VOICE_Z  			SYS_VOICE_Y+ 1		 	 //系统字母提示音编号
#define SYS_VOICE_VOLUMEMAX  	SYS_VOICE_Z+ 1		 	 //音量已达最大提示音编号
#define SYS_VOICE_VOLUMEMIN  	SYS_VOICE_VOLUMEMAX+ 1	 //音量已达最小提示音编号
#define SYS_VOICE_RECSTART  	SYS_VOICE_VOLUMEMIN+ 1	 //
#define SYS_VOICE_RECSTOP  		SYS_VOICE_RECSTART+ 1	 //
#define SYS_VOICE_MP3MODE  		SYS_VOICE_RECSTOP+ 1	 //
#define SYS_VOICE_OIDMODE  		SYS_VOICE_MP3MODE+ 1	 //
#define SYS_VOICE_RFMMODE  		SYS_VOICE_OIDMODE+ 1	 //read after me mode sound
#define SYS_VOICE_EXITGAME 		SYS_VOICE_RFMMODE+ 1	 //read after me mode sound
#define SYS_VOICE_DIY_RECMODE 	SYS_VOICE_EXITGAME+ 1	 //read after me mode sound
//#define SYS_VOICE_DIY_RECMODE 	SYS_VOICE_MP3MODE	 //read after me mode sound
#define SYS_VOICE_DIY_PLAYMODE 	SYS_VOICE_DIY_RECMODE + 1	 //read after me mode sound
//#define SYS_VOICE_DIY_PLAYMODE 	SYS_VOICE_MP3MODE	 //read after me mode sound
#define SYS_VOICE_DIY_RECSTART 	SYS_VOICE_DIY_PLAYMODE+ 1	 //read after me mode sound
//#define SYS_VOICE_DIY_RECSTART 	SYS_VOICE_RECSTART 	//SYS_VOICE_DIY_PLAYMODE+ 1	 //read after me mode sound
#define SYS_VOICE_DIY_RECSTOP 	SYS_VOICE_DIY_RECSTART+ 1	 //read after me mode sound
//#define SYS_VOICE_DIY_RECSTOP 	SYS_VOICE_RECSTOP 	//SYS_VOICE_DIY_RECSTART+ 1	 //read after me mode sound
#define SYS_VOICE_DIY_NOREC 	SYS_VOICE_DIY_RECSTOP+ 1	 //read after me mode sound
//#define SYS_VOICE_DIY_NOREC 	SYS_VOICE_RECSTOP 		//SYS_VOICE_DIY_RECSTOP+ 1	 //read after me mode sound
//#define
#define SYS_VOICE_INVALID_INDEX 	SYS_VOICE_DIY_NOREC+ 1	 //read after me mode sound
//#define SYS_VOICE_DIY_NOREC 	SYS_VOICE_RECSTOP //SYS_VOICE_DIY_RECSTOP+ 1	 //read after me mode sound
#define SYS_VOICE_NOSPACE 		SYS_VOICE_INVALID_INDEX	+ 1	 //
#define SYS_VOICE_IDLE_WARNING	SYS_VOICE_NOSPACE	+ 1	 //


//spi sound num
enum SPISOUND
{
	ENUM_SPI_BLE_ON = 0X8000,	//蓝牙开启音
	ENUM_SPI_INS_CARD ,
	ENUM_SPI_TEST ,
	ENUM_SPI_BLE_DOWN ,
	ENUM_SPI_PREV ,
	ENUM_SPI_NEXT ,				//5
	ENUM_SPI_EN_AUTO_OFF ,
	ENUM_SPI_DIS_AUTO_OFF ,
	ENUM_SPI_WELCOME ,
	ENUM_SPI_UPDATE_FW_START ,
	ENUM_SPI_UPDATE_FW_END ,	//10
	ENUM_SPI_UPDATE_FW_FAIL ,
	ENUM_SPI_IDLE_WARNING_3MIN ,
	ENUM_SPI_SPEED_NORMAL ,
	ENUM_SPI_SPEED_SLOW1 ,
	ENUM_SPI_RECORD_START ,		//录音播放前加播放的提示音abc mouse
	ENUM_SPI_INVALID_WARNING ,

};
//-----------------------------------------------------------------------------------------------------------------------------------------


//-----------------------------------------------------------------------------------------------------------------------------------------
//GetUserSetting用来取设定值的ID
#define LOW_BATTERY_VALUE 		7		//低电压设定值取值标识
#define POWER_ON_TIME 			5		//默认开机时间设定值取值标识
#define POWER_DOWN_TIME 		2		//设定的按键关机时间
#define AUTO_POWER_DOWN 		1		//默认自动机时间设定值取值标识
#define DEFAULT_SPEED_LEVEL 	27 		//默认速度等级设定值取值标识



//变量定义
extern u16  g_uiVolumeTbl[MAX_VOLUME_LEVEL] ;	//= {0,5,10,15,20,24,27,30,33,36,40,43,46,50,60,70,80,181};
extern u16 g_uiVolumeLevelCnt;				//设定的音量等级总数
extern u16 g_uiActVolIdx;						//Volume Index of g_uiVolumeTbl[].指示当前系统的音量级别
//speed control								//		0.5	0.75    0     1.25    1.5
extern int  g_uiSpeedTbl[MAX_SPEED_LEVEL] ;	//= {0x2000,0x3000,16384,0x5000,0x6000,};
extern u16 g_uiSpeedLevelCnt;				//设定的音速等级总数
extern u16 g_uiActSpeedIdx;					//指示当前系统的音速级别

extern FS_HANDLE pSysFile;					//不与mp3共用，以便在来回切换时，还在原来的文件上
extern FS_HANDLE pIMARecFile;				// Recording file handle pointer.

//
extern u16 g_uiAutoPowerDownTimes; 			//用于保存用户设定的自动关机时间
extern WORD g_uiOnOffKeyPressTime;	//用于保存用户设定的开关机按键时间，1/8ms

extern u16 SDCard_FAT_LIST[256];

//-----------------------------------------------------------------------------------------------------------------------------------------
//Function:
//	GetSystemSoundInfo
//Description:
//	取得系统提示音的开始及结束位置
//Input:
//	FS_HANDLE pSysFile 					：系统文件的handle
//	WORD iSoundNumber 			：要取得的声音编号，从0开始
//OutPut:
//	u32 *dwMp3PlayStartAddr 	：声音在文件中的开始位置
//	u32 *dwMp3PlayEndAddr 	：声音在文件中的结束位置
//Return:
//
//Others:
//
//-----------------------------------------------------------------------------------------------------------------------------------------
extern void GetSystemSoundInfo(FS_HANDLE pSysFile,u32 iSoundNumber,u32 *dwMp3PlayStartAddr,u32 *dwMp3PlayEndAddr);

//-----------------------------------------------------------------------------------------------------------------------------------------
//Function:
//	OidSystemSettingInit
//Description:
//	初始系统设定信息，并检查，打开系统设定文件，取得文件句柄
//Input:
//
//OutPut:
//	g_uiVolumeLevelCnt，g_uiActVolIdx，g_uiVolumeTbl，g_uiSpeedLevelCnt，g_uiActVolIdx，g_uiSpeedTbl
//	g_uiAutoPowerDownTimes，g_uiOnOffKeyPressTime
//Return:
//	0	: 打开文件失败
//	非0	: 文件handle,打开文件成功
//Others:
//
//-----------------------------------------------------------------------------------------------------------------------------------------
extern FS_HANDLE  *OidSystemSettingInit(void);


//-----------------------------------------------------------------------------------------------------------------------------------------
//Function:
//	OidSysFileOpen
//Description:
//	打开系统设定文件，取得文件句柄
//Input:
//
//OutPut:
//
//Return:
//	0	: 打开文件失败
//	非0	: 文件handle,打开文件成功
//Others:
//
//-----------------------------------------------------------------------------------------------------------------------------------------
extern FS_HANDLE  *OidSysFileOpen(void);//路径可以改为作为参数传入，目前固定

//-----------------------------------------------------------------------------------------------------------------------------------------
//Function:
//	GetDefaultBookCode
//Description:
//	取得默认书本码
//Input:
//	FS_HANDLE pSysFile	：系统文件handle
//OutPut:
//
//Return:
//	0xFFFF 		:无设定默认书码
//	0-0xFFFE	:设定书码值
//Others:
//
//-----------------------------------------------------------------------------------------------------------------------------------------
extern u32 GetDefaultBookCode(FS_HANDLE pSysFile);

//-----------------------------------------------------------------------------------------------------------------------------------------
//Function:
//	FS_FReadSectorSizeData
//Description:
//	从任意位置开始读出一个sector(512B)数据。
//Input:
//	FS_HANDLE pFile 					：所要读取的文件的handle
//	u32 dwByteStartAddr 	：要读取内容的文件开始位置。
//OutPut:
//	WORD *pTemp 			：接收读出数据的buffer
//Return:
//
//Others:
//
//-----------------------------------------------------------------------------------------------------------------------------------------
extern WORD FS_FReadSectorSizeData(WORD wLen,WORD wDecType,WORD *pTemp,FS_HANDLE pFile,DWORD dwByteStartAddr);


//-----------------------------------------------------------------------------------------------------------------------------------------
//Function:
//	SetVolumeLevel
//Description:
//	设定系统音量等级
//Input:
//	WORD iVolumeLevle		：要设定的音量等级
//OutPut:
//
//Return:
//
//Others:
//
//-----------------------------------------------------------------------------------------------------------------------------------------
extern u16 SetVolumeLevel(WORD iVolumeLevle);



//-----------------------------------------------------------------------------------------------------------------------------------------
//Function:
//	GetUserSetting
//Description:
//	从系统文件取回取回设定设定
//Input:
//	FS_HANDLE pSysFile 				：所要读取的文件的handle
//	u32 dwByteStartAddr 	：要读取内容的文件开始位置。
//	FS_HANDLE pSysFile 				：系统文件的handle
//	WORD iWordSettingID		：要读取的第几个设定值
//	//	iWordSettingID 可取的值，
//	  	LOW_BATTERY_VALUE 	7		//
//		POWER_ON_TIME 		5		//
//	  	POWER_DOWN_TIME 	2		//
//		AUTO_POWER_DOWN 	1		//
//		DEFAULT_SPEED_LEVEL 27		//
//OutPut:
//	WORD *iSettingValue 	：保存结果值
//Return:
//	0	：失败
//	非0	：成功
//Others:
//
//-----------------------------------------------------------------------------------------------------------------------------------------
extern u16 GetUserSetting(FS_HANDLE pSysFile,WORD iWordSettingID,WORD *iSettingValue);


//-----------------------------------------------------------------------------------------------------------------------------------------
//Function:
//	DecSectorData
//Description:
//	解密512B数据
//Input:
//	u32 dwByteStartAddr	：要设定的音量等级
//	WORD wDecType			：解密类型
//	WORD *pDecDataBuf		：要解密的数据buffer
//OutPut:
//	WORD *pDecDataBuf		：要解密的数据buffer
//Return:
//
//Others:
//
//-----------------------------------------------------------------------------------------------------------------------------------------
extern void DecSectorData(WORD wByteOffsetSec,u32 dwByteStartAddr,WORD wDecType,WORD *pDecDataBuf);

extern void GetDecKeyData(void);



extern WORD IndexNeedPlaySound(void);



//-----------------------------------------------------------------------------------------------------------------------------------------
//Function:
//	BookInit
//Description:
//	做一此必要的初始化，主要为dlo来回切换时，保证切换回来可以正常跑
//Input:
//	FS_HANDLE pFile					：当前正常打开的书本文件指针
//OutPut:
//	pPlayFile
//Return:
//	0 : init成功
//	非0 ：init不成功
//Others:
//
//-----------------------------------------------------------------------------------------------------------------------------------------
WORD BookInit(FS_HANDLE pFile);



//-----------------------------------------------------------------------------------------------------------------------------------------
//Function:
//	GetHandWriteHitSoundGroup
//Description:
//	获取书码所要播放的声音组编号，
//Input:
//	FS_HANDLE pFile					：书本文件的handle
//	WORD iSuceessOrFail		：只能是0，1	，0表示成功的声音，1为失败的声音
//OutPut:
//	全局参数：
//	g_u32IndexSoundGroup			：声音编号保存于此数据中
//	g_iNeedPlaySoundCnt;			//需要播放的声音个数
//	g_iOidPlayActiveSoundNo;		//=0，当前正在播放index 的第几个声音，从0号开始播放
//Return:
// 	0 ：书本无效，或是index没有定义声音
// 	1 ：获取声音号成功
//Others:
//	若要此功能，需要打才可以，(ENABLE_GET_HAND_WRITE_SOUND)
//-----------------------------------------------------------------------------------------------------------------------------------------
extern WORD GetHandWriteHitSoundGroup(FS_HANDLE pFile,WORD iSuceessOrFail);





#if ENABLE_SPEED_LOW
//-----------------------------------------------------------------------------------------------------------------------------------------
//Function:
//	SpeedRiseUp
//Description:
//	系统升频到48M
//Input:
//
//OutPut:
//
//Return:
//
//Others:
//
//-----------------------------------------------------------------------------------------------------------------------------------------
extern void SpeedRiseUp(void);

//-----------------------------------------------------------------------------------------------------------------------------------------
//Function:
//	SpeedFallDown
//Description:
//	降频
//Input:
//	no
//OutPut:
//
//Return:
//
//Others:
//
//-----------------------------------------------------------------------------------------------------------------------------------------
void SpeedFallDown(void);

#endif


#endif
