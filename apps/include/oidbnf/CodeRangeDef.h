
#ifndef __CODE_RANGE_DEF__
#define __CODE_RANGE_DEF__


//常数定义
#define CODE_EXIT_DEF 				52006	//退出码
#define CODE_DIY_REC_MODE 			52896	//diy录音模式进入码
#define CODE_DIY_REC_PLAY_MODE 		52898	//diy录音播放模式进入码
#define CODE_DIY_REC_EXIT 			52897	//diy录音/播放模式退出码

#define CODE_DEL_WAV				65347	//录音删除码

//Dic phycial index def
#define DIC_PHY_IDX_ENTRY	 65396
#define DIC_PHY_IDX_EXIT 	65397
#define DIC_PHY_IDX_DEL		65398
#define DIC_PHY_IDX_ENTER	65399
#define DIC_PHY_IDX_SPEC	65527
#define DIC_PHY_IDX_LINK	65526
#define DIC_PHY_IDX_A		65089
#define DIC_PHY_IDX_Z		65114
//变量定义


#endif