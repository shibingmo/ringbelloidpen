
#ifndef __EANBEL_FUNC_DEF__
#define __EANBEL_FUNC_DEF__

//常数定义
//-----------------------------------------------------------------------------------------------------------------------------------------
//function enable or not
//各家不同功能定义可单独文件再在OidIndexSoundFunctionLib.h中include即可
#define USE_AIDEDDLO 				0	// 1 USE,0 NO
#define USE_AIDEDDLO_GAME_SEL 		0	// 1 USE,0 NO
#define ENABLE_LCD_SUPPORT 			0	// 1 USE,0 NO，support lcd show
#define ENABLE_SEL_DIRECTORY 		1 	// 1 USE,0 NO,sel book no need book code
#define ENABLE_DIY_SOUND 			1	// 1 USE,0 NO,enable DIY mode
#define ENABLE_SEND_INDEX 			1 	// 1 USE,0 NO,enable send index to ble,2.4G or others,save space
#define ENABLE_SEND_DONOTCARE_INDEX 0 	// 1 USE,0 NO,need send send don't care index or not
#define ENABLE_UPDATE_END_SOUND 	1 	// 1 USE,0 NO,若要播放更新完成提示音，External_Patch.c cann't in app_prj
#define ENABLE_UPDATE_LIST 			1 	// 1 USE,0 NO,when no find bnf in tf,need update list or not
#define ENABLE_CHIP_INGNORE 		0 	// 1 USE,0 NO,ingnore chip id for test
#define ENABLE_RECORD_GAME 			0 	// 1 USE,0 NO,need 录音游戏 or not
#define ENABLE_TIMEOUT_GAME_DIY 	0 	// 1 USE,0 NO
#define ENABLE_MP3_NUMBER_BACKUP 	0 	// 1 USE,0 NO,是否保存最后播放的mp3，以便下次开机从此mp3开始
#define ENABLE_SPEED_LOW 			0	// 1 USE,0 NO,是否需降速
#define ENABLE_SERIAL_INDEX_CHG_SCE 0 	// 1 USE,0 NO ,serial same index ,loop scenario play soud
#define ENABLE_VOLUMN_BACKUP 		1 	// 1 USE,0 NO ,save the volumn to spi
#define ENABLE_WR_CONTENT_TO_SPI 	0 	// 1 USE,0 NO ,在点读过程中保存index，key等内容到 spi
#define ENABLE_CARD_BOOK 			1 	// 1 USE,0 NO ,debug mode 
#define ENABLE_UPDATE_PWDOFF 		0 	// 1 USE,0 NO ,when update finish, power down or not
#define ENABLE_DOWN_SPEED_RECORD	1	// 1 USE,0 NO ,when rec,down to 24MHz or not
#define ENABLE_DIC_MODE				0	// 1 USE,0 NO ,字典，暂无
#define ENABLE_DIY_REC_CARD_MODE	0	// 1 USE,0 NO ,录音卡，暂无
#define ENABLE_OUTPUT_ERR_CODE		0	// 1 USE,0 NO ,录音卡，暂无
#define ENABLE_GET_HAND_WRITE_SOUND	0	// 1 USE,0 NO ,取得书本中手写相关提示音
#define ENABLE_SEL_BK_FAIL_KEEP_PRE	0	// 1 USE,0 NO ,找不到书是否保留之前打开的书本
#define ENABLE_GLOBAL_FAT_LIST_BUF	1	// 1 USE,0 NO ,使scanfat的buf为global的，否则要在各个需录音的DLO中作声名并scan
#define ENABLE_TESTMODE 			0	// 1 USE,0 NO
#define ENABLE_UPDATE_FW_SD			1
#define ENABLE_BACKGROUND_MIDI		0  // 1 USE,0 NO
#define ENABLE_RS232_TX				0  // 1 USE,0 NO
#define ENABLE_SCE_AUTO_LOOP		1  // 1 USE,0 NO
#define ENABLE_SCE_AUTO_LOOP_IDXS	1  // 1 USE,0 NO
#define ENABLE_REC_CARD				1  // 1 USE,0 NO
#define ENABLE_ABCM_SPECIAL_GAME	0  // 1 USE,0 NO，特别处理游戏码
#define ENABLE_ABCM_REMEM_SCENARIO	1  // 1 USE,0 NO,换书后，仍在上一本书的相应scenario下
#define ENABLE_ABCM_REMEM_MINORMAX	1  // 1 USE,0 NO,记下要找书码值大的还是小的书
#define ENABLE_ABC_RIGHT_QUES_EXIT	1  // 1 USE,0 NO,答对一个问题就退出
#define ENABLE_ABC_SEL_QUES_SUCESS	1  // 1 USE,0 NO,答对一个问题就退出
#define ENABLE_KEEP_LAYER_STATUS	1  // 1 USE,0 NO,换后书，仍保持layer不变，如果当前的layer大于总数，用默认的
#define ENABLE_ABC_DEMO_DEF_24G		1  // 1 USE,0 NO,开机后默认设置到2.4G模式
#define ENABLE_ABC_DEMO_GAME_SET	1  // 1 USE,0 NO,for abc mouse demo,因新数据来不及，直接在FW中设定好相应的游戏特别设定
#define ENABLE_ABC_EXTERN_CTRL_PWR	1  // 1 USE,0 NO,外部控制电源，送出通知，已可关闭电源
#define ENABLE_ABC_NOBOOK_WARNING	1  // 1 USE,0 NO,外部控制电源，送出通知，已可关闭电源





//-----------------------------------------------------------------------------------------------------------------------------------------

//edit id,
//0 ：None
//1	: STD
//2	: 
//3	: 学献
//4	：联云格
#define EDITID 						1



#endif//#ifndef __EANBEL_FUNC_DEF__
