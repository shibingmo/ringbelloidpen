一、在使用本库解析bnf数据流程如下

//OidIndexSoundFunctionLib.h
1、要先检查数据合法性，CheckBookFile()	
2、检查合法后，再初始化数据 InitBookFile(file,EDITID) 
才可以开始对数据提示行操作



二、如何取得一个码的对应声音编号组，如下
1、要先获取此码的类型按码的类型确定相应操作
GetIndexType()						//ParsingIndexTypeLib.h
2、按类型获取相应的声音或执行动如
switch(indexType)
{
	//若为内容码
	case Idx_ELEMENT:
		//获取内容码的声音组
		GetIndexSoundGroup()

	//若为内书码
	case Idx_BOOK :
	case Idx_BOOKOID2 :
	case Idx_CARDBOOK :
		//凭此书码找到相就书本，并打开相应书本，
		//检查通过后，取回此书本码对应音进行播放
		GetBookSoundGroup()
		//注意，如为Idx_CARDBOOK，则打开书本后，将此码再次作为新收到的码，在所打开的书本中找到相应内容播放
		
	//若为游戏码
	case Idx_GAME :
		//则要先获取游戏类型，再凭游戏类型，看要切换到那个游戏中
		GetGameType()
		//本库目前只支持以下一个类型
		#define GAME_MANY_GROUP 			0X0100	//问题有各自提示音游戏

		//切换到游戏中后，要先初始化游戏
		GetGameInitMGR()
		//此后要一直调用game游戏处理函数
		GameMGR()	//GameManyGroupSound.h
		
	//若场景码
	case Idx_SCENARIO :
		//则切换场景，并同时获取场景声音播放
		SetScenario（）
		GetScenarioSoundGroup()

	//若layer code
	case Idx_LayerCode :
		//则切换layer，并同时获取layer声音播放
		SetLayer（）
		GetLayerSoundGroup()

	//若功能码，则切换功能，或是自定也可
	case Idx_FUNC :
		//或为功能码，则会有一个在功能码内的logic 值，此值对应的功能码如IndexFunctionCodeDef.h
		//中的FUNC_xxxx所示
	
}

3、2中所取得的此码的声音编号组（即可能是一个，或是多个）此组声音编号保存于一个数组中
//  g_u32IndexSoundGroup            ：声音编号保存于此数组中
//  g_iNeedPlaySoundCnt;            //需要播放的声音个数
//  g_iOidPlayActiveSoundNo;        //=0，当前正在播放index 的第几个声音，从0号开始播放



三、如何取得声音在数据中的位置及播放过程中解码
1、用所得到的声音编号，再从数据中取得此声音编号在数据中的address
	GetSoundInfo()
	再依取得的地址即可进行声音播放
	
2、依1中的位置读得声音数据后，在进行一次解码，才可得到原始声音数据，用以下fucntion解码
	AfterMP3FillBuffer()
	//注，此function每次只解512B
	



四、以下为我们游戏调用的demo code，供参考

//初始化
void MainDloGameMgrInit(void)
{

	GetGameInitMGR(pOidPlayFile);
	{
		//停止声音播放，
		OidStopAllPlaying();
		//播放进入game提示音
		GetGameEntrySoundMGR(pOidPlayFile);
		//有则进入game mode
	}
}

//游戏主流程
void MainDloGameMgr(void)
{
	//MSG msgRx = g_msgApp;
	MSG msg;
	unsigned int uiIndexType = 0;
	unsigned int uiIndexLogicValue = 0;
	u32 ulPhysicalIndexValue = 0;
	

	GameSoundPlayMGR();
	//
	

	//查看当前声音是否允许插断，不允许则将点到的index清除
	if((d_uiMGRSoundPlaySeqCtl & SPSC_BIT15_1) != 0)
	{
		//点码不许插断当前声音
	    if(g_msgApp.uiApcType ==  APC_TYPE_INDEX_TYPE  || (g_msgApp.uiApcType = 
APC_TYPE_VALID_INDEX))

	    {
			g_msgApp.uiApcType = APC_TYPE_NONE;
		}

	}
	//检查是否是有效index消息
    if(g_msgApp.uiApcType == APC_TYPE_INDEX_TYPE )
	{
		//取得所点的有效index
		uiIndexType = g_msgApp.uiSubType;
		uiIndexLogicValue = g_msgApp.ulData2;
		ulPhysicalIndexValue = g_msgApp.ulData1;
	}
	
	//game流程
	if(!GameMGR(pOidPlayFile,&uiIndexType,uiIndexLogicValue,ulPhysicalIndexValue))
	{
		//game完成后退出
		//led 长亮
		//切mode
		g_uiLed1CtrlType = LED1_TYPE_LIGHT;
		msg.uiApcType = APC_TYPE_MODE_CHG;
		msg.uiSubType = OID_PLAY_MODE;
		SetAppFifo(msg);
		return;
	}

	//拦下点到的index,以解决在播放声音时点码切换在caca音
	GameIndexProcessMGR();



}

void GameSoundPlayMGR(void)
{
	//MSG msgRx = g_msgApp;
	MSG msg;
    if((MP3_GetStatus( ) != MP3_CMD_NO))
	{
		//当前有声音在播放，直接返回
		return;
	}

	//for 点到书本音，并放到buffer后，被game中处理
    if (g_iOidPlayActiveSoundNo < g_iNeedPlaySoundCnt )
    {
		//有声音要播放
		msg.uiApcType = APC_TYPE_MP3_PLAY;
		msg.uiSubType = SUB_MP3_PLAY_GAME;
		if((g_u32IndexSoundGroup[g_iOidPlayActiveSoundNo % INDEX_MULTI_SOUND_MAX] & 
0x8000) ==0)

		{
			//local
			GetGameSoundInfoMGR(pOidPlayFile,g_u32IndexSoundGroup[
g_iOidPlayActiveSoundNo % INDEX_MULTI_SOUND_MAX],&msg.ulData1,&msg.ulData2); 
//获取声音号对应的声音信息，开始地址及size


		}
		else
		{
			//表示此声音为game global中的声音
			//global
			GetGameGlobalSoundInfoMGR(pOidPlayFile,g_u32IndexSoundGroup[
g_iOidPlayActiveSoundNo+1 % INDEX_MULTI_SOUND_MAX],&msg.ulData1,&msg.ulData2)
; //获取声音号对应的声音信息，开始地址及size

			g_iOidPlayActiveSoundNo++;
		}

		//启动mp3播放
		MnMp3PlayStart(msg);
        g_iOidPlayActiveSoundNo++;


    }

}

