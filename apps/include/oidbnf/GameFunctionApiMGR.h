#ifndef __GAME_MGR_API_H__
#define __GAME_MGR_API_H__

#include "OidIndexSoundFunctionLib.h"

//类型定义------------------------------------------------------------------------------------
typedef enum gameResulttab
{
	GAME_RESULT_SUCCUSS_ALL=1,	//所有问题都答对
	GAME_RESULT_FAIL_ALL,		//所有问题都答错
	GAME_RESULT_SUCCUSS_PARTIAL,	//一部份问题所有问题都答对
}GAME_RESULT;

//常数定义------------------------------------------------------------------------------------
#define MAX_CORRECT_ANSWER  25//15  //70033中data ram少了，210个，所以要减少
#define MAX_SPECIAL_ANSWER  1//35//15
#define MAX_HAVE_ANSWER     8//10//10,1bit表示一个问题，所以到128个
#define MAX_QUESTION_CNT    50//8//10//for mitun change to 50,资料够可以放大

//用于控制声音先后播放顺序，如播放完奖励音后再播放正确声音等
//d_uiMGRSoundPlaySeqCtl bit使用，
//bit15 ,control question sound not interrupt
#define SPSC_BIT15_1        0X8000
#define SPSC_BIT15_0        0X7FFF


#define GET_MULTI_SOUND_FLAG_ALL 		0XFFFF 		//WORD

//变量定义------------------------------------------------------------------------------------
//game
extern u16 d_uiMGRTotalQuestion;
extern u16 d_uiMGRNeedAnswerQuestion;
extern u16 d_uiMGRTimeOut;
extern u16 d_uiMGRTimeOutExitGame;
extern u16 d_uiMGRAllowAnswerErrorTime;     //允许答错次数
extern u16 d_uiMGRPlayControlFlag;
extern u16 d_uiMGRAnswerType;
extern u16 d_uiMGRNeedAnswerCorrectCnt;     //答对几个就算通过。

extern u16 d_uiMGRTimeOutTime;              //记录第几次到达idle time 时间，由于有些需要不同的idletime播放不一样的提示音

extern u16 d_uiMGRAnswerWrongTime;          //记录答错次数
extern u16 d_uiMGRSpecialAnswerWrongTime;   //魁疭岿粇Ω计  //Chunchia 20150116 add for test
extern u16 d_uiMGRActiveQuestionNo;         //当前正在玩的问题
extern u32 d_ulMGRCorrectAnswerIndex[MAX_CORRECT_ANSWER];   //保存问题的答案值
extern u16 d_uiMGRCorrectAnswerIndexCnt;
extern u32 d_ulMGRSpecialAnswerIndex[MAX_SPECIAL_ANSWER];   //保存特别答案码
extern u16 d_uiMGRSpecialAnswerIndexCnt;
extern u16 d_uiMGRHavedAnswerIndex[4];      //保存已回答过的答案值
extern u16 d_uiMGRHavedAnswerIndexCount;    //已回答案问题总数计数
//改为one bit 来表示是否是已回答过，最多为50个答案，所以需4 word
//0 :表示未答
//1 ：表示已答
extern u16 d_uiMGRHavedAnsQuesCnt; 			//目前已回答的问题数
extern u16 d_uiMGRActiveAnswerNo;           //当前正在回答的第几个答案

//，此游戏并没有点码提问，所以可以省出来
extern u16 d_uiMGRQuestionArray[MAX_QUESTION_CNT];      //问题数组MAX_QUESTION_CNT
extern u16 d_uiMGRHavedAnswerQuestion[MAX_HAVE_ANSWER]; //标识已回答过的问题，点码选问题用，此时，d_uiMGRQuestionArray中应保存每个问题的问题进入码，且按顺序
//改为one bit 来表示是否是已回答过，最多为256个问题，所以需64 word,
//0 :表示未答
//1 ：表示已答

extern u16 d_uiMGRExitGameFlag;         //退出游戏用，0:表示答错即退出，=需答题数:全部回答正确，<需答题数:部份回答正确
extern u16 d_uiMGRSoundPlaySeqCtl;      //用于控制声音先后播放顺序，如播放完奖励音后再播放正确声音等
extern u32 d_uiMGRuIndexValue;          //

//为修改特殊错误移入问题及增加相应扩展功能
extern u16 d_uiMGRBookGameParam1;   //data table的前面的size,
extern u16 d_uiMGRBookGameParam2;         // question项的总大小，
extern u16 d_uiMGRQuestionControlFlag;  // question的控制相关项，
extern u16 d_uiMGRGameTableVersion;     // 本游戏数据的版本，

extern u16 d_wClickAnsTime; 		//
extern u16 d_wSetJiangLiYin; 		//


//函数定义------------------------------------------------------------------------------------
void GetSoundGroupFromGameMultiTableMGR(FS_HANDLE pFile,u16 SoundNoOffset);
u16 IsHavedAnswerMGR(WORD AnswerType, u32 Index);
void InsertToHavedAnswerMGR(WORD AnsSequence);
u16 IsCorrectAnswerMGR(WORD AnswerType, WORD AnswerSeq, u32 Index);
u16 IsSpecialAnswerMGR(FS_HANDLE pFile,u32 Index);
//extern void GenerateRandomNumber(WORD TotalQuestion);
void GenerateRandomQuestionNumberSeqMGR(WORD TotalQuestion);
void ResetHavedAnswerMGR(void);


u16 IsQuestionIndexMGR(u32 uIndex);
void InsertToHavedSelQuestionMGR(WORD uQuestionSeq);
u16 IsHaveSelQuestionMGR(WORD uQuestionSeq);

//game 相关API
u16 GetGameInitMGR(FS_HANDLE pFile,u32 dwPhysicalGameIndex);

//game 中sound no.大多为word编号，此处用DW，可以与index sound共用同一个array
//WORD IndexValue, 点到的index实际值，只为中文拼字，因点到声韵母进入游戏，及游戏码进入游戏
void GetGameEntrySoundMGR(FS_HANDLE pFile);
u16 GetGameGlobalSoundInfoMGR(FS_HANDLE pFile,u32 u32SoundNumber, u32 *dwMp3PlayStartAddr, u32 *dwMp3PlayEndAddr);
u16 GetGameSoundInfoMGR(FS_HANDLE pFile,u32 u32SoundNumber, u32 *dwMp3PlayStartAddr, u32 *dwMp3PlayEndAddr);

//问题内容初始化
void InitQuestionMGR(FS_HANDLE pFile);
//取得正确答案码的提示音
void GetCorrectAnswerSoundMGR(FS_HANDLE pFile,WORD AnsSequence);
void GetSpecialAnswerSoundMGR(FS_HANDLE pFile,WORD AnsSequence,WORD xSound);
void GetWrongAnswerSoundMGR(FS_HANDLE pFile);
void GetCorrectCompleteQuestionSoundMGR(FS_HANDLE pFile);
void GetWrongCompleteQuestionSoundMGR(FS_HANDLE pFile);
void GetHavedAnswerSoundMGR(FS_HANDLE pFile,WORD xSound);
//答题失败退出
void GetQuestionFailExitSoundMGR(FS_HANDLE pFile);
//全部答对退出
void GetAllSuccessExitSoundMGR(FS_HANDLE pFile);
//部份答对退出
void GetNotAllSuccessExitSoundMGR(FS_HANDLE pFile);
//次数从1开始，
void GetIdleTimeSoundMGR(FS_HANDLE pFile,WORD IdleTimeTime);
//
void GetIdleTimeEixtGameSoundMGR(FS_HANDLE pFile);
//取提出错后，再次提问问题的提示音
//ErrTime 错误次数，传入值从0算起
void GetQuetionAskSoundMGR(FS_HANDLE pFile,WORD ErrTime);
//取提一个随机奖励音
void GetRandomRewardSoundMGR(FS_HANDLE pFile);


extern WORD GetGameSoundGroupBaseOnSoundNoMGR(FS_HANDLE pFile,WORD uSoundNumber,WORD xSound);

//随机取问题号
extern WORD GetRandomQuestionNumberSeqMGR(WORD TotalQuestion);

//看答案码是否在答案里面
//for abc mouse spell word game
//0 no
//1 yes
u16 IsInCorrectAnswerRangeMGR(u32 Index);
u16 IsInSpecialAnswerRangeMGR(u32 Index);

void GetActiveQuesNoAndQuesCntMGR(WORD *wActQuesNumber,WORD *wQuesCnt);
void GetIdleTimeSetSecMGR(WORD *widleTimeOut,WORD *widleTimeExitGame);

//在游戏退出后，再调用此函数获取结果，看是答对了还是答错了
GAME_RESULT GameGetGameResult();
void SetRandomValueMGR(u32 dwRandomValue);

//返回当前问题号，从0开始
WORD Oid_GetActiveQuestionNoMGR(void);
//返回当前问题的答案总个数
WORD Oid_GetActiveQuestionAnsWerCntMGR(void);

//返回当前问题的正确答案，u32 *pdwAnswerBuf接收答案的buffer,WORD AnswerBufLen buffer的长度
WORD Oid_GetActiveQuestionAnsWerMGR(u32 *pdwAnswerBuf,WORD wAnswerBufLen);

#endif
