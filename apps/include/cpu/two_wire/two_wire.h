#ifndef __TWO_WIRE_H__
#define __TWO_WIRE_H__

#include "typedef.h"
#include "common/common.h"

#define tw_delay()      delay(100)//7.6us

#define TW_PORT			JL_PORTA

#define TW_SCK				2
#define TW_SDIO				1

#define LOW                 0
#define HIGH                1

#define tw_clk_out()    do{TW_PORT->DIR &= ~BIT(TW_SCK);TW_PORT->PU &= ~BIT(TW_SCK);}while(0)
#define tw_clk_in()     do{TW_PORT->DIR |=  BIT(TW_SCK);TW_PORT->PU |=  BIT(TW_SCK);TW_PORT->PD &= ~BIT(TW_SCK);}while(0)
#define tw_clk_h()      do{TW_PORT->OUT |=  BIT(TW_SCK);TW_PORT->DIR &=~BIT(TW_SCK);}while(0)
#define tw_clk_l()      do{TW_PORT->OUT &= ~BIT(TW_SCK);TW_PORT->DIR &=~BIT(TW_SCK);}while(0)


#define tw_data_out()   do{TW_PORT->DIR &= ~BIT(TW_SDIO);TW_PORT->PU &= ~BIT(TW_SDIO);}while(0)
#define tw_data_in()    do{TW_PORT->DIR |=  BIT(TW_SDIO);TW_PORT->PU |=  BIT(TW_SDIO);TW_PORT->PD &= ~BIT(TW_SDIO);}while(0)
#define tw_data_r()     (TW_PORT->IN&BIT(TW_SDIO))
#define tw_data_h()     do{TW_PORT->OUT |=  BIT(TW_SDIO);TW_PORT->DIR &= ~BIT(TW_SDIO);}while(0)
#define tw_data_l()     do{TW_PORT->OUT &= ~BIT(TW_SDIO);TW_PORT->DIR &= ~BIT(TW_SDIO);}while(0)


void master_send_nbyte(u8 *cmd, u8 len);
void master_send_onebyte(u8 data);
void master_setup_slave_init();
bool master_readfrom_slave(u8 *data);

#endif // __TWO_WIRE_H__
