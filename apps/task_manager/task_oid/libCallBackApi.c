/*-----------------------------------------------------------------------------------------------------------------------------------------
FileDiscribe
FileName	:
Author		:Qin
Data		:
Email		:
Description	:
Version		:
Hardware&IDE:
Copyright(C),SONIX TECHNOLOGY Co.,Ltd.
History		:
//---------------------------------------------------------------------------------------------------------------------------------------*/



//#include "mdi_audio.h"

#include "OidIndexSoundFunctionLib.h"
#include "SystemSoundPlay.h"
//#include "SystemInfoOffsetDef.h"
#include "ParsingIndexTypeLib.h"
#include "GameVariableDef.h"


#include "music_decoder.h"
#include "fs_io.h"




//-----------------------------------------------------------------------------------------------------------------------------------------
//Function:
//  OidStopAllPlaying
//Description:
//  停止所有当前声音播放动作及未完成播放的动作
//Input:
//
//OutPut:
//
//Return:
//
//Others:
// 要能停止声音播放，游戏流程接口中调用此停止声音播放，以便进入一下步骤的声音播放
//-----------------------------------------------------------------------------------------------------------------------------------------
void OidStopAllPlaying(void)
{
    //Stop Recording and Playback
    //要调用平台的停止声音播放函数
	music_decoder_stop_two();
}


//-----------------------------------------------------------------------------------------------------------------------------------------
//Function:
//  MP3_GetStatus
//Description:
//  侦测mp3是否有在播放
//Input:
//
//OutPut:
//
//Return:
//  0 :无声音播放
//	1 :有声音在播放
//Others:
// 要准确返回当前播放的状态，游戏流程接口中调用此判断是否有声音播放，以决定是否进入一下步骤的声音播放
//-----------------------------------------------------------------------------------------------------------------------------------------
WORD MP3_GetStatus(void)
{
	WORD ret = 1;

    //要调用平台的当前播放状态
	//if((ret = mdi_audio_is_playing(MDI_AUDIO_PLAY_ANY)) == 0)
	if(music_decoder_get_status_two() != MUSIC_DECODER_ST_PLAY)
	{
		ret = MUSIC_DECODER_ST_STOP;
	}

	return ret;
}

//获取当前文件指针位置
//  FS_HANDLE pFile 		/* [IN] File object */
//  u32 *dwPreFilePos,   	/* [IN] 保存文件指针结果 */
//返回值 1成功，0失败
WORD FS_GetFilePosition(FS_HANDLE pFile,u32 *dwPreFilePos)
{
	WORD ret = 1;

	*dwPreFilePos = fs_tell(pFile);

	return ret;
}

//移动文件指针
//  FS_HANDLE pFile 	/* [IN] File object */
//  u32 dwSeekAddr,   /* [IN] File read/write pointer */
//  u32 dwSeekWay	SEEK_SET, SEEK_CUR, SEEK_END/* [IN] 从头/当前/结尾seek到需要的位置， */
//返回值 1成功，0失败
WORD FS_Seek(FS_HANDLE pFile,u32 dwSeekAddr,u32 dwSeekWay)
{
	DWORD dwRet=0;

    dwRet = fs_seek(pFile,dwSeekWay,dwSeekAddr);

	if(g_wLibDebugOutputCtrl == ERR_LIB_DEBUG_INIT_BOK_FILE)
	{
		printf("fs_seek() pFile=%d,sekRet%d,addr=0x%x\r\n",(DWORD)pFile,dwRet,dwSeekAddr);
	}

	return dwRet;
}

//从文件中读取内容
//按平台的实际操作API,实现此function,lib中会调用此function来读回文件内容，
//  FS_HANDLE pFile 	/* [IN] File object */
//  void* buff,  		/* [OUT] Buffer to store read data */
//  u32 dwReadLen,    /* [IN] Number of bytes to read */
//  u32 *dwBr     	/* [OUT] Number of bytes read */
//返回值 1成功，0失败
WORD FS_Read(FS_HANDLE pFile,BYTE *pbReadBuf, u32 dwReadLen,u32 *dwBr)
{
	WORD ret = 1;

	*dwBr = fs_read(pFile,pbReadBuf,dwReadLen);

	return ret;
}

//返回文件长度
DWORD FS_GetFileLen(FS_HANDLE pFile)
{
	DWORD f_size = 0;

    fs_io_ctrl(NULL, pFile, FS_IO_GET_FILE_SIZE, &f_size);

	return f_size ;
}

