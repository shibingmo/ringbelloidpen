#ifndef OIDPEN_FUNCTION_H_INCLUDED
#define OIDPEN_FUNCTION_H_INCLUDED

#include "OidIndexSoundFunctionLib.h"

#define AUTO_POWEROFF_TIME 1000   //s

extern u32 m_dwOIDPhyIndex;
extern u32 m_dwStartAddr;
extern u32 m_dwEndAddr;
extern _FIL_HDL bnflist_hdl;

void BuildBnfFileList(MUSIC_PLAYER* obj);
WORD FindFileByBnfFileList(_FS_HDL *fs_hdl, _FIL_HDL *f_hdl, WORD wIndexType,u32 dwOidPhyIndex);
void parse_receive_data(u8 *data);
MUSIC_PLAYER *oid_bnfplayer_start(void);
tbool oidpen_play_bnffile(MUSIC_PLAYER *obj, u32 filenum);
MUSIC_PLAYER *  oidpen_play_tone(MUSIC_PLAYER *obj, u8 index);
void oid_msg_filter(int *msg);
void oid_bnfplayer_stop(void *priv);
void reset_poweroff_time(void);
u32 oid_receive_end_parse_data();
tbool check_turnto_oid_mode();
#endif // OIDPEN_FUNCTION_H_INCLUDED
