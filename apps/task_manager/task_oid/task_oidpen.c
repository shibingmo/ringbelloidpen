#include "file_operate.h"
#include "file_op_err.h"
#include "sdk_cfg.h"
#include "task_manager.h"
#include "task_common.h"
#include "task_oidpen_key.h"
#include "two_wire.h"
#include "msg.h"
#include "warning_tone.h"
#include "music_player.h"
#include "fat_io.h"
#include "file_io.h"
#include "dev_manage.h"

#include "asm_type.h"
#include "audio/audio.h"
#include "adc_api.h"
#include "dac.h"
#include "clock.h"
#include "oidpen_function.h"
#include "task_oidpen.h"
#include "rec_api.h"

#include "OidIndexSoundFunctionLib.h"
#include "SystemSoundPlay.h"
#include "ParsingIndexTypeLib.h"
#include "GameVariableDef.h"
#include "GameManyGroupSound.h"
#include "GameFunctionApiMGR.h"

extern tbool task_music_skip_check(void **priv);

WORD g_wActiveMode = OID_PLAY_MODE; //默认在点读模式
FS_HANDLE pOidPlayFile = NULL;      // open BNF file
FS_HANDLE pSystemFile  = NULL;      //打开系统文件(SystemSetting.bin/OID Code Settings.XML)

WORD m_wIndexType;
u32 m_dwIndexLogic;
DWORD g_dwPreElementPhyIndex;
BYTE m_isPlayAllOpen;

WORD m_wGameType;

//idle要自设添加，不在ilde中计时										<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
WORD m_wIdleTimeOutSet;
WORD m_wIdleTimeExitGameSet;
WORD m_wIdleTimeX;
WORD m_wIdleTimeCnt;
extern BYTE oidpen_play_type;
#if OID_REC_EN
    extern BYTE oidpen_play_type;
    RECORD_OP_API *oid_rec_api = NULL;
void oidrec_mutex_init(void *priv)
{
    log_printf("oidrec_mutex_init...\n");
}

void oidrec_mutex_stop(void *priv)
{
    log_printf("oidrec_mutex_stop...\n");
    rec_exit(&oid_rec_api);
}
#endif // OID_REC_EN

tbool task_oid_check_sd0(void **priv)
{
    log_printf("task_oid_check_sd0 !!\n");

    //DEV_HANDLE *dev = *priv;
    u32 parm;
    u32 dev_status;

    tbool dev_cnt = 0;

    printf("oid *priv:d0x%x\n", *priv);
    //check some device online
    if ((*priv != PASS_HI_TONE)||(*priv == NULL))
    {
          //  if (!dev_get_online_status(sd1, &dev_status))
            {
             //   if (dev_status != DEV_ONLINE)
                {  log_printf("task_oid_check_sd0 run here...\n");
                    *priv = dev_get_fisrt(MUSIC_DEV_TYPE, DEV_ONLINE);
                }

            }

    }

        printf("oid *priv:d0x%x\n", *priv);
        if (*priv == sd0) {
            puts("[PRIV SD0]\n");
        } else if (*priv == sd1) {
            puts("[PRIV SD1]\n");
        } else if (*priv == usb) {
            puts("[PRIV USB]\n");
        } else {
            puts("[PRIV CACHE]\n");
        }

        if (*priv == NULL) {
            return false;
        } else {
            return true;
        }


    //check specific device online
    return true;
}


static void *task_oidpen_init(void *priv)
{
    log_printf("\n.........%s in ........\n\n\n",__func__);

    MUSIC_PLAYER *obj = NULL;

	dac_channel_on(MUSIC_CHANNEL, 0);
    set_sys_vol(sound.vol.sys_vol_l, sound.vol.sys_vol_r, FADE_ON);

    obj = oid_bnfplayer_start();

    //建立书本列表
	//>>要根据实际情况选择建立的机时，如文件较多，这个时间可能会比较久
	BuildBnfFileList(obj);

    audio_set_output_buf_limit(OUTPUT_BUF_LIMIT_MUSIC_TASK);

    if (priv != PASS_HI_TONE){
        oidpen_play_tone(obj, TONE_OID_HI);
    }
    else
        m_dwOIDPhyIndex = 0xffffffff;
    return obj;
}


static void task_oidpen_exit(void **priv)
{
    music_player_destroy((MUSIC_PLAYER **)priv);
    audio_set_output_buf_limit(OUTPUT_BUF_LIMIT);
    set_sys_freq(SYS_Hz);
    task_clear_all_message();
    oid_bnfplayer_stop(*priv);

#if OID_REC_EN
    rec_exit(&oid_rec_api);
    mutex_resource_release("record_play");
    //mutex_resource_release("oid_rec");
#endif
}

void task_oidpen_deal(void *hdl)
{
    log_printf("%s in .................\n\n\n",__func__);

    WORD index = 0, found_ret = 0xFFFF;
    WORD tmp_rec_time;
	WORD wRet;
    int msg = NO_MSG;
    int msg_error = MSG_NO_ERROR;
    u8 oid_start = 0;
    WORD opened_bnfnumber=0;
    pOidPlayFile = NULL;

	MUSIC_PLAYER *obj = (MUSIC_PLAYER *)hdl;

    while(TRUE)
    {
        msg_error = task_get_msg(0, 1, &msg);

        if (get_tone_status())  //提示音还未播完前过滤涉及解码器操作的消息
        {
            oid_msg_filter(&msg);
        }

        if (task_common_msg_deal(hdl, msg) == false)   //平台消息处理函数
        {
            log_printf("msg exit, return ...............\n");
            break;
        }
        #if OID_REC_EN
        rec_msg_deal_api(&oid_rec_api, msg); //record 流程
        #endif

        oid_receive_end_parse_data();//接收-解析笔头数据

            if(m_dwOIDPhyIndex != 0xffffffff) //解析的码为无效，不往下执行
            {

               if( ((m_dwOIDPhyIndex >= 0x01) && (m_dwOIDPhyIndex <= 0x4A38)) \
               || ((m_dwOIDPhyIndex >= 0x5209) && (m_dwOIDPhyIndex <= 0xEFFF)) )//select BOOK
                {
                    m_wIndexType = Idx_BOOKOID2;
                }
                else
                {
                    if(!pOidPlayFile)//第一次进入,如果进入到这里提示先读书本封面。
                    {
                        oidpen_play_tone(obj, TONE_RFBOOK);
                        m_dwOIDPhyIndex = 0xffffffff;
                        m_wIndexType = 0;

                        continue;
                    }

                    if((opened_bnfnumber != file_operate_get_file_number(obj->fop)) && opened_bnfnumber)
                    {   log_printf("opened_num2 = %d,file_number=%d\n", opened_bnfnumber, file_operate_get_file_number(obj->fop));
                        file_operate_set_file_number(obj->fop, opened_bnfnumber);
                        wRet = file_operate_op(obj->fop, FOP_OPEN_FILE_BYNUM, NULL, NULL);
                        pOidPlayFile = (_FIL_HDL *)file_operate_get_file_hdl(obj->fop);
                        opened_bnfnumber = file_operate_get_file_number(obj->fop);
                    }

                    GetIndexType(pOidPlayFile, NULL, m_dwOIDPhyIndex,&m_wIndexType,&m_dwIndexLogic); //点书码后才有pOidPlayFile
                }
                log_printf("m_wIndexType:0x%04X\n", m_wIndexType);

            }


/**************************************start switch(m_wIndexType)*****************************************************/
        switch(m_wIndexType)    //根据码点类型作相应的操作
        {
            //切书
            case Idx_BOOK    :
            case Idx_BOOKOID2:
            case Idx_CARDBOOK:
            {

				//找到m_dwOIDPhyIndex对应的书本，并打开，并检查书本合法性，然后初始化
				//查找m_dwOIDPhyIndex对应的文件
				/*每本书都有三个信息可以打开它，OID3BOOKCODE，OID2BOOKCODE，RANGE[][]
				如果类型为Idx_BOOK，则用m_dwOIDPhyIndex跟每有书本的OID3BOOKCODE比对
				如果类型为Idx_BOOKOID2，则用m_dwOIDPhyIndex跟每有书本的OID2BOOKCODE比对
				如果类型为Idx_CARDBOOK，则用m_dwOIDPhyIndex跟每有书本的RANGE比对
				如此即可找开对应的书本文件
				*/
				//到列表中查找，此index对应是那一个书本文件(bnf)
				found_ret = FindFileByBnfFileList(&(obj->fop->fop_file->fs_hdl), &bnflist_hdl, m_wIndexType, m_dwOIDPhyIndex);
				if( found_ret != 0xffff)
				{
				    index = found_ret; //found a new book
					//在列表中找到了书本，//打开书本文件
                    file_operate_set_file_number(obj->fop, (index));

                    OidStopAllPlaying();//正在解码时操作打开文件概率性死机
                    wRet = file_operate_op(obj->fop, FOP_OPEN_FILE_BYNUM, NULL, NULL);
                    if(!wRet)
                        log_printf("open file OK...\n");
                    else
                        log_printf("open file err...wRet = %d\n",wRet);

                    pOidPlayFile = (_FIL_HDL *)file_operate_get_file_hdl(obj->fop);
                    opened_bnfnumber = file_operate_get_file_number(obj->fop);
                    if(!pOidPlayFile)
                    {
                        break;
                    }

					wRet = CheckBookFile(pOidPlayFile);	//检查书本
                    log_printf("wRet : %d \n", wRet);

					if(wRet == 0)
					{
						wRet = InitBookFile(pOidPlayFile);//检查通过，初始化
                        log_printf("wRet : %d \n", wRet);

						if(wRet == 1)
						{
							if(m_wIndexType == Idx_CARDBOOK)//初始化成功，找书本声音播放
							{
								//卡片书切换完成后，第一次点的码再次用码值，去取得它在该书中的类型
								//GetIndexType(pOidPlayFile, pSystemFile, m_dwOIDPhyIndex,&m_wIndexType,&m_dwIndexLogic);
								//清除收到的码值
								//m_dwOIDPhyIndex = 0xffffffff;
							}
							else
							{
								GetBookSoundGroup(pOidPlayFile);
								/*有声音要播放时，都是通过以下全局变量来通知
								g_iOidPlayActiveSoundNo >>从第几个声音开机，默认是0
								g_iNeedPlaySoundCnt 	>>总共有几个声音要播放
								>>g_u32IndexSoundGroup[]  >>所有要播放的声音号
								*/
								//清除收到的码值
								log_printf("g_iOidPlayActiveSoundNo:%d\n", g_iOidPlayActiveSoundNo);
								log_printf("g_iNeedPlaySoundCnt:%d\n", g_iNeedPlaySoundCnt);
								int i;
								for(i = 0; i < g_iNeedPlaySoundCnt; i++)
                                    log_printf("g_u32IndexSoundGroup:%d\n", g_u32IndexSoundGroup[i]);

								m_dwOIDPhyIndex = 0xffffffff;
							}

							g_wActiveMode = OID_PLAY_MODE;//如当前在其它模式，切换书本后，回到点读模式
							OidStopAllPlaying();//播放完没有设置停止状态，导致切书不能播放
						}
						else
						{
							//无法初始化成功，播放提示失败音
							//清除收到的码值
							m_dwOIDPhyIndex = 0xffffffff;
						}

					}
					else
					{
						//检查不通过，提示失败
                        oidpen_play_tone(obj, TONE_CANTRD);

						//清除收到的码值
						m_dwOIDPhyIndex = 0xffffffff;
					}
				}
				else
                {
                    oidpen_play_tone(obj, TONE_CANTRD);
                }

				m_wIndexType = 0;//清除类型
				break;
			}

            //切换游戏
            case Idx_GAME:
            {
				m_wGameType = GetGameType(pOidPlayFile);//获取游戏类型，
				//按游戏类型，先行初始化游戏
				switch(m_wGameType)
				{
					case GAME_MANY_GROUP:
					{
						//停止声音播放，
						OidStopAllPlaying();
						//
						GetGameInitMGR(pOidPlayFile,m_dwOIDPhyIndex);
						//播放进入game提示音
						GetGameEntrySoundMGR(pOidPlayFile);
						/*>>所有声音要播放时，都是通过以下全局变量来通知
						  >>g_iOidPlayActiveSoundNo >>从第几个声音开机，默认是0
						  >>g_iNeedPlaySoundCnt 	>>总共有几个声音要播放
						  >>g_u32IndexSoundGroup[]	>>所有要播放的声音号
						*/

						//idle要自行添加，因要ilde计时										<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
						//check idle
						GetIdleTimeSetSecMGR(& m_wIdleTimeOutSet,& m_wIdleTimeExitGameSet);//获取数据中设定的idle时间及idle退出时间
						//切mode
						g_wActiveMode = GAME_PLAY_MODE;//游戏
						break;

					}
				}

				//清除类型
				m_wIndexType = 0;
				//清除收到的码值
				m_dwOIDPhyIndex = 0xffffffff;


                break;
            }

            default:
                break;
        }
/**************************************end switch(m_wIndexType)*****************************************************/

/**************************************start switch(g_wActiveMode)*****************************************************/
        switch(g_wActiveMode)   //看当前是哪个模式，走对应模式的流程
        {
            case OID_PLAY_MODE:
            {
				switch(m_wIndexType)	//按码值类型处理，看是要播放声音还是要做动作
				{
					case Idx_ELEMENT :
					{
						OidStopAllPlaying();    //如有声音播放，先停止声音

						if(GetIndexSoundGroup(pOidPlayFile, m_dwOIDPhyIndex))   //取得内容码的声音，并播放
						{

						}

						g_dwPreElementPhyIndex = m_dwOIDPhyIndex;   //保存值
						m_wIndexType = 0;   //清除类型
						m_dwOIDPhyIndex = 0xffffffff;   //清除收到的码值

						break;
					}

					case Idx_SCENARIO :  //场景码
					{
						SetScenario(m_dwIndexLogic);
						GetScenarioSoundGroup(pOidPlayFile,m_dwIndexLogic);

						m_wIndexType = 0;   //清除类型
						m_dwOIDPhyIndex = 0xffffffff;   //清除收到的码值

						break;
					}

					case Idx_LayerCode :   //Layer码
					{
						SetLayer(m_dwIndexLogic);
						GetLayerSoundGroup(pOidPlayFile,m_dwIndexLogic);

						m_wIndexType = 0;   //清除类型
						m_dwOIDPhyIndex = 0xffffffff;   //清除收到的码值

						break;
					}
					////》》》》》》》》》》》》》》》》》》》》》》》》》》》》》》》》》》》》》》》》》》》》》》
					//DIY录音及播放码可以在bnf中指定那些码作为DIY
					//DIY录音及DIY播放，都有记录它在DIY录音或DIY播放中，是第几个（logic），以便DIY录音与DIY播放配组使用
					//它们的顺序（logic）是按他们的码值大小各自排序
					case Idx_DIYRecord :  //DIY录音码
					{
						DWORD dwLogicDIY;

						GetLastIndexSoundGroup(pOidPlayFile);   //取得此码对应的声音

						dwLogicDIY = m_dwIndexLogic;    //此DIY码在本书中是第几组，(0,1,2)，可与DIY录音播放码配对
						m_wIndexType = 0;   //清除类型
						m_dwOIDPhyIndex = 0xffffffff;   //清除收到的码值

						break;  //准备启动录音等动作
					}

					case Idx_DIYRecordPLAY :   //DIY录音播放码
					{
						DWORD dwLogicDIY;

						GetLastIndexSoundGroup(pOidPlayFile);   //取得此码对应的声音

						dwLogicDIY = m_dwIndexLogic;    //此DIY播放码在本书中是第几组，(0,1,2)，可与DIY录音码配对
						m_wIndexType = 0;               //清除类型
						m_dwOIDPhyIndex = 0xffffffff;   //清除收到的码值

						break;  //准备启动录音播放等动作
					}

					case Idx_FUNC :     //功能码
					{
			            //if(m_dwIndexLogic == FUNC_CHANGE_SCENERIO )
			            {
			                //切换scenario
			                //g_wScenarioActive += 1;
			                //if(g_wScenarioActive >= g_wScenarioCnt)
			                {
			                    //
			                //    g_wScenarioActive = 0;
			                }
			                //切换scenario,
			             //   SetScenario(g_wScenarioActive);
			                //播放声音
			                //有声音要播放，停止声音播放，没有则继续播放原来的声音。
			             //   OidStopAllPlaying();
				         //   if(GetScenarioSoundGroup(pOidPlayFile, g_wScenarioActive))
			                {
			                }
							//去掉此功能

			                //按理应该是断掉之前的声音才对
			            }
			            if(m_dwIndexLogic == FUNC_TRANSLATION )
			            {
			                //翻译
			                if(g_dwPreElementPhyIndex != 0xffffffff)
			                {
								//获取翻译声音内容
								GetElementIdxTransSoundGroup(pOidPlayFile, g_dwPreElementPhyIndex);
			                }
			            }

						if(m_dwIndexLogic == FUNC_REPEAT )
						{
							//repeat
							if(g_dwPreElementPhyIndex != 0xffffffff)
							{
								//有声音要播放，停止声音播放，
								OidStopAllPlaying();
								if(GetIndexSoundGroup(pOidPlayFile, g_dwPreElementPhyIndex))
								{
								}
								//repeate mode en
							}
						}

						if(m_dwIndexLogic == FUNC_PLAY_ALL )
						{
							unsigned int SoundCount;    //play all

							OidStopAllPlaying();    //停止声音播放，
							GetPlayAllSoundCount(pOidPlayFile, &g_iNeedPlaySoundCnt);	//取总数

							g_iOidPlayActiveSoundNo = 0;	//从第0个开始
							GetPlayAllSoundNoGroup(pOidPlayFile, g_iOidPlayActiveSoundNo);
							m_isPlayAllOpen = true;
						}

						m_wIndexType = 0;   //清除类型
						m_dwOIDPhyIndex = 0xffffffff;	//清除收到的码值

						break;
					}

					case Idx_NeedSelectBook:
  					{
  					    m_wIndexType = 0;   //清除类型
						m_dwOIDPhyIndex = 0xffffffff;   //清除收到的码值

                        oidpen_play_tone(obj, TONE_RFBOOK);
                        break;
  					}

					default :   //其它类型处理
					    m_wIndexType = 0;   //清除类型
						m_dwOIDPhyIndex = 0xffffffff;   //清除收到的码值

					    break;
				}

				//声音播放处理, 启动声音播放--------------------------------------------------------------------------------------------
                //log_printf("mp3_status:%d, index:%d\n",MP3_GetStatus(), index );
				if(MP3_GetStatus() == MUSIC_DECODER_ST_STOP) /*是否有声音在播放*/
				{
					//检查当前无声音播放，以便有多个声音时，按顺序播放完
					//看是否有声音需要播放,
					/*有声音要播放时，都是通过以下全局变量来通知
					g_iOidPlayActiveSoundNo >>从第几个声音开机，默认是0
					g_iNeedPlaySoundCnt		>>总共有几个声音要播放
					>>g_u32IndexSoundGroup[]  >>所有要播放的声音号
					*/
					//log_printf("g_iOidPlayActiveSoundNo:%d, g_iNeedPlaySoundCnt:%d\n",g_iOidPlayActiveSoundNo, g_iNeedPlaySoundCnt );
					if (g_iOidPlayActiveSoundNo < g_iNeedPlaySoundCnt)
					{
						u32 dwSoundNum;
						//有声音要播放
						//应要清除别的所有声音播放buffer.

						//上一个声音播放过完，且所要播放的声音没有播放完，播放下一个。
						//作些更改，为play all,因声音最大才开五十，所有声音是会超过50的。
						if ((g_iOidPlayActiveSoundNo > 0) || ((g_iOidPlayActiveSoundNo == 0) && ((g_u32IndexSoundGroup[g_iOidPlayActiveSoundNo % INDEX_MULTI_SOUND_MAX] & 0x80000000) == 0)))
						{

						}
						else
						{
							g_iOidPlayActiveSoundNo++; //如最高位为1，则数组中下一个值才是声音号 //
						}
						/*
                            据当前模式，将声音号发送出来，以便在各个dlo中其自行决定如何查得此声音number的start及end addr
                            取得内容码声音的开始及结束位置
                            准备播放下一个声音
                            启动声音播放
						*/
						dwSoundNum = g_u32IndexSoundGroup[g_iOidPlayActiveSoundNo % INDEX_MULTI_SOUND_MAX];
						GetSoundInfo(pOidPlayFile, dwSoundNum, &m_dwStartAddr, &m_dwEndAddr);
						g_iOidPlayActiveSoundNo++;

						log_printf("dwSoundNum:%d, m_dwStartAddr:0x%08X, m_dwEndAddr:0x%08X\n",dwSoundNum, m_dwStartAddr, m_dwEndAddr);
                        if(m_dwStartAddr < m_dwEndAddr)
                            oidpen_play_bnffile(obj, index);

						//for palyall>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
						if(m_isPlayAllOpen == true)
						{
							if(g_iOidPlayActiveSoundNo % INDEX_MULTI_SOUND_MAX == 0)
							{
								//取下一批,
								//目前play all超过 g_iOidPlayActiveSoundNo[] buffer大小
								//所以要循环取值
								//unsigned int SoundCount;
								GetPlayAllSoundNoGroup(pOidPlayFile, g_iOidPlayActiveSoundNo);
							}
						}
						//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
					}
					else
					{
						//for palyall>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
						if(m_isPlayAllOpen == true)
						{
							m_isPlayAllOpen = false;
                        }
						//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
					}
				}

				break;
			}//case OID_PLAY_MODE:
            case OID_READAFTER_MODE:
            case GAME_PLAY_MODE:
            case MP3_PLAY_MODE:
                break;
            default:
                break;
        }
/***************************************end switch(g_wActiveMode)****************************************************/

/**************************************start switch(msg)*****************************************************/
        switch (msg)
        {
            case SYS_EVENT_DEC_END:
                if(tone_var.status)
                {
                    tone_var.status = 0;
                    tone_var.idx    = 0;
                }
                oidpen_play_type = 0;
                log_printf("DEC_END...\n");
                break;
            case SYS_EVENT_PLAY_SEL_END:
                 log_printf("SYS_EVENT_PLAY_TONE_END\n");
                 tone_var.status = 0;
                 tone_var.idx    = 0;
                 oidpen_play_type = 0;
                 break;
            case MSG_VOL_UP:
                if(oidpen_play_type == 1)
                    break;
                if(sound.vol.sys_vol_l == get_max_sys_vol(0))
                    oidpen_play_tone(obj, TONE_VOL_MAX);
                else
                    oidpen_play_tone(obj, TONE_VOL_UP);
                break;
            case MSG_VOL_DOWN:

                if(oidpen_play_type == 1)
                    break;

                if(get_sys_vol(0)==10)
                    oidpen_play_tone(obj, TONE_VOL_MIN);
                else
                    oidpen_play_tone(obj, TONE_VOL_DOWN);
                break;
            case MSG_POWER_OFF:
                {
                            //         log_printf("MSG_POWER_OFFSYS_EVENT_PLAY_TONE_END。。。。。。。。。。。。。。。\n");
                }
                break;
            case MSG_SD0_OFFLINE:
                fs_close(&bnflist_hdl);
                break;
            case MSG_SD0_ONLINE:
                BuildBnfFileList(obj);
                break;


            #if OID_REC_EN
            case MSG_REC_INIT:
                log_printf("MSG_REC_INIT\n");
                break;

            case MSG_REC_START:
                oidpen_play_type=0;
                dac_set_samplerate(48000, 0);
                break;

            case MSG_HALF_SECOND:
                    tmp_rec_time = rec_get_enc_time(oid_rec_api);
                    if (tmp_rec_time) {
                        log_printf("rec time %d:%d\n", tmp_rec_time / 60, tmp_rec_time % 60);
                    }
                break;
            #endif // OID_REC_EN
            default:
                break;
        }
/**************************************end switch(msg)*****************************************************/
    }//end while(TRUE)

    fs_close(&bnflist_hdl);
}



const TASK_APP task_oidpen_info = {
    .skip_check = task_oid_check_sd0,
    .init 		= task_oidpen_init,
    .exit 		= task_oidpen_exit,
    .task 		= task_oidpen_deal,
    .key 		= &task_oidpen_key,
};
